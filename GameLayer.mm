//
//  GameLayer.m
//  Be A Square
//
//  Created by Ziga Dolar on 5/17/13.
//  Copyright Upalela 2013. All rights reserved.
//


// Import the interfaces
#import "GameLayer.h"
#import "SelectMenu.h"
#import "Main.h"

// Math
#import "Vec2.h"
#import "Vec3.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#pragma mark - GameLayer

#pragma mark - definitions


#define ARC4RANDOM_MAX      0x100000000
#define odmikBarveBG    3
#define odmikBarve      15
#define gradMult   3


#pragma mark TESTING SETUP and retinaCheck

bool _testingMode = false; //TRUE for alpha testing

CGSize winSize;
int gameModeInt = 0;

bool _heroClip = true;      //if TRUE hero hits walls
bool _enemyClip = false;    //if TRUE enemy hits walls

bool gh = true;             //if TRUE, GM_COLLECT has falling collectibles - guitar hero style

/*
 @interface GameLayer (PrivateMethods)
 
 //private methods
 - (void)pauseGame;
 
 @end
 */


// GameLayer implementation
@implementation GameLayer

// Helper class method that creates a Scene with the GameLayer as the only child.
+(CCScene *) scene:(int)mode
{
    
    gameModeInt = mode;
    
    // 'scene' is an autorelease object.
    CCScene *scene = [CCScene node];
	
    // 'layer' is an autorelease object.
    GameLayer *layer = [GameLayer node];
    layer.tag = 123456789;
	
    // add layer as a child to scene
    [scene addChild: layer];
	
    // return the scene
    return scene;
}


//Pause when going out of the app
//if the game isn't over - pause the game when user hits the Home button/is interupted
-(void) doPauseWhenLeavingGame{
    
    if (gameOver != YES) {
        [self pauseGame];
    }
    
}


//Pause game function
bool isPause = NO;
-(void) pauseGame{
    
    
    countdownNum = 5;
    isStart = NO;
    accelUpdated = NO;
    gameOver = YES;
    isPause = YES;
    
    
    //
    //we're in pause mode - enable auto-lock timer
    //
    
    [self changeAutoLock];
    
    
    CCArray *loopArray = [[mainGameLayer children] copy];
    
    
    for (id obj in loopArray) {
        
        if ([obj isMemberOfClass:[subSprite class]]) {
            
            
            subSprite *sprite = (subSprite*)obj;
            
            //NSLog(@"sprite with tag %d and description %@", sprite.tag, sprite.description);
            if (sprite != NULL && sprite.visible != NO) {
                
                if (sprite.tag == 60) {
                    
                    [[[CCDirector sharedDirector] actionManager] pauseTarget:sprite];
                    
                }
            }
        }
    }
    
    
    
    [self doPause];
    
}


//Resume Game
//resuming game mode - disable auto-lock timer
-(void) resumeGame {
    
    
    [self changeAutoLock];
    
    [pauseLayer removeAllChildrenWithCleanup:YES];
    [self removeChild:pauseLayer cleanup:YES];
    
    gameOver = NO;
    
}

//Resume after pause
//UNPAUSE
-(void) resumeAllActionsAfterPause {
    
    
    CCArray *loopArray = [[mainGameLayer children] copy];
    
    
    for (id obj in loopArray) {
        
        if ([obj isMemberOfClass:[subSprite class]]) {
            
            
            subSprite *sprite = (subSprite*)obj;
            
            if (sprite != NULL && sprite.visible != NO) {
                
                if (sprite.tag == 60) {
                    
                    [[[CCDirector sharedDirector] actionManager] resumeTarget:sprite];
                    
                }
            }
        }
    }
    
    
}

//Restart game mode which is played
-(void) restart {
    
    
    timerCount = 0;
    
    int mode = 0;
    
    switch (gameMode) {
        case GM_COLLECT:
            mode = 1;
            break;
            
        case GM_SURVIVE:
            mode = 2;
            break;
            
        case GM_CLASSIC:
            mode = 3;
            break;
            
        default:
            break;
    }
    
    [[CCDirector sharedDirector] replaceScene:[GameLayer scene:mode]];
}


//Back to Select Game Mode Menu
-(void) backToMenu {
    
    
    timerCount = 0;
    
    [[CCDirector sharedDirector] replaceScene: [SelectMenuLayer scene]];
    
    
}



#pragma mark - Game mode parameters defaults

int pixelSize = 32; //surviver mode default

int steviloPoX = 15; // 480/32
int steviloPoY = 10; // 320/32

bool gameOver = NO;
bool isStart = NO;
int countdownNum = 3;
int countdownCount = 10;


#pragma mark - Game mode specific parameters

//GM_COLLECT parameters
int scoreCount = 0;


#define minTime 45  //minimum spawn time
#define maxTime 120  //maximum spawn time
#define gameLengthSeconds 60 //60 - minuta
int gameTimeFrames = 0;



// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {
		
        
        //
        //we're in game mode - disable auto-lock timer
        //
        
        [self changeAutoLock];
        
        //
        //////
        //
        
        //google analytics tracker
        id <GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        if (_testingMode != true) {
            [tracker sendView:[NSString stringWithFormat:@"GameLayer_mode_%d", gameModeInt]];
        } else {
            [tracker sendView:[NSString stringWithFormat:@"aTest_GameLayer_mode_%d", gameModeInt]];
        }
        
        
        
        
        //clear texture cache before game starts
        
        //Use these
        [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFrames];
        [[CCSpriteFrameCache sharedSpriteFrameCache] removeUnusedSpriteFrames];
        
        //Use these
        [[CCTextureCache sharedTextureCache] removeUnusedTextures];
        [[CCTextureCache sharedTextureCache] removeAllTextures];
        [[CCDirector sharedDirector] purgeCachedData];
        
        
        //define colors
        
        
        //default colors
        //_bgColor1 = ccc3(125,125,125);
        //_bgColor1 = ccc3(225, 225, 225);
        _bgColor1 = ccc3(40,40,40);
        //_bgColor1 = ccc3(237, 84, 114);
        _bgColor2 = ccc3(222, 222, 222);
        _heroColor = ccc3(237, 84, 114);
        //_heroColor = ccc3(225,225,225);
        _enemyColor = ccc3(150, 228, 97);
        //_enemyColor = ccc3(25,25,25);
        _consumableColor = ccc3(244, 130, 156);
        
        _hudColor1 = ccc3(175, 175, 175);
        
        
        //another color scheme - TO DO WHEN USER SWITCHES/BUYS NEW COLORS
        
        //         _bgColor1 = ccc3BFromHex(0x101010);
        //         _bgColor2 = ccc3BFromHex(0x050505);
        //         _heroColor = ccc3BFromHex(0xFF3300);
        //         _enemyColor = ccc3BFromHex(0x0033FF);
        //         _consumableColor = ccc3BFromHex(0x803380);
        //
        //         _hudColor1 = ccc3BFromHex(0xFFFFFF);
        
        
        
        //////
        //Set GameMode parameters depending on which game mode we are playing
        
        switch (gameModeInt) {
            case 1:
                gameMode = GM_COLLECT;
                //pixelSize = 40;
                pixelSize = 32;
                gh = true;
                break;
                
            case 2:
                gameMode = GM_SURVIVE;
                pixelSize = 32;
                gh = false;
                break;
                
            case 3:
                gameMode = GM_CLASSIC;
                //pixelSize = 20;
                pixelSize = 32;
                gh = false;
                
                break;
                
            default:
                break;
        }
        
        winSize = [[CCDirector sharedDirector] winSize];
        
        
        CGSize size = CGSizeMake(480,320);
        //CGSize size = CGSizeMake(480,256);
        
        mainGameLayer = [CCLayerColor layerWithColor:ccc4(_bgColor1.r, _bgColor1.g, _bgColor1.b, 255) width:size.width height:size.height];
        mainGameLayer.anchorPoint = ccp(0,0);
        mainGameLayer.position = ccp((winSize.width-size.width)/2,(winSize.height-size.height)/2);
        
        
        [self addChild:mainGameLayer z:100];
        
        
        
        //calculate grid based on pixel size
        
        steviloPoX = size.width/pixelSize;
        steviloPoY = size.height/pixelSize;//ceil((size.height-40)/pixelSize);
        
        //
        //////
        
        
        switch (gameMode) {
            case GM_COLLECT:
                //pixelSize = 40; //pixel size
                
                //calculate game time for timed game
                gameTimeFrames = (int) (gameLengthSeconds*60.0f);
                
                
                break;
                
            case GM_SURVIVE:
                //pixelSize = 32; //pixel size
                //[self spawnEnemy];
                
                break;
                
            case GM_CLASSIC:
                //pixelSize = 20; //pixel size
                [self spawnEnemy];
                
                break;
                
            default:
                break;
        }
        
        
        
        
        self.isTouchEnabled = YES; //add touch component to layer
        
        
        //enable accelerometer
        self.isAccelerometerEnabled = YES;
        UIAccelerometer*  theAccelerometer = [UIAccelerometer sharedAccelerometer];
        theAccelerometer.updateInterval = 1 / 10.0f;
        theAccelerometer.delegate = self;
        
        
        
        //make background
        [self genBackground];
        
        //make gameLayerBackground
        [self genGameLayerBackground];
        
        //create gui
        [self createHud];
        [self createMenu];
        [self createPowerupMenu];
        
        //make Hero
        [self spawnSelf];
        
        
        //update game loop
        [self scheduleUpdate];
        
	}
	return self;
}


//Create a game hud with SCORE and time COUNTDOWN
-(void) createHud {
    
    CGSize size = winSize;
    
    
    HUDlayer = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 0) width:winSize.width height:winSize.height];
    HUDlayer.anchorPoint = ccp(0,0);
    HUDlayer.position = ccp(0,0);
    
    
    [self addChild:HUDlayer z:5000];
    
    
    countdownLabel = [CCLabelTTF labelWithString:@" " fontName:@"04b03" fontSize:200.0f];
    countdownLabel.color = _hudColor1;
    countdownLabel.visible = false;
    countdownLabel.scale = 1.0;
    
    countdownLabel.position = ccp(size.width/2,size.height/2);
    
    [HUDlayer addChild:countdownLabel z:10000];
    
    
    if (gameMode != GM_SURVIVE) {
        
        int startScore = 0;
        
        scoreLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", startScore] fontName:@"04b03" fontSize:22.0f];
        scoreLabel.anchorPoint = ccp(0,0.5);
        scoreLabel.position = ccp(15, size.height-15);
        scoreLabel.color = _hudColor1;
        
        [HUDlayer addChild:scoreLabel z: 10000];
        
    }
    
    
    
    
    if (gameMode == GM_COLLECT) {
        
        int framesToSeconds = (int)floor(gameTimeFrames/60);
        
        int minutes = (int)floor(framesToSeconds/60);
        
        int seconds = framesToSeconds - minutes*60;
        
        
        timerLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d:%02d", minutes, seconds] fontName:@"04b03" fontSize:22.0f];
        
        timerLabel.color = _hudColor1;
        
        timerLabel.position = ccp(size.width/2, size.height-15);
        
        [HUDlayer addChild:timerLabel z:10000];
        
    } else {
        
        int framesToSeconds = (int)floor(timerCount/60);
        
        int minutes = (int)floor(framesToSeconds/60);
        
        int seconds = framesToSeconds - minutes*60;
        
        
        timerLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d:%02d", minutes, seconds] fontName:@"04b03" fontSize:22.0f];
        
        timerLabel.color = _hudColor1;
        
        timerLabel.position = ccp(size.width/2, size.height-15);
        
        [HUDlayer addChild:timerLabel z:10000];
        
    }
    
}



//Pause button
-(void) createMenu {
    
    CGSize size = CGSizeMake(480,320);
    
    
    CCMenuItemFont *pauseButton = [CCMenuItemFont itemWithString:@"pause" target:self selector:@selector(pauseGame)];
    pauseButton.fontName = @"04b03";
    pauseButton.fontSize = 22;
    
    pauseButton.position = ccp(size.width-35, size.height-15);
    
    pauseButton.color = _hudColor1;
    
    
    CCMenu *menu = [CCMenu menuWithItems:pauseButton, nil];
    
    menu.position = ccp(0,0);
    
    [self addChild:menu z:2000];
    
}

//Power up buttons displayed in game layer
-(void) createPowerupMenu {
    
    CGSize size = CGSizeMake(480,320);
    
    
    CCMenuItemFont *powerupButton;
    
    if (gameMode == GM_COLLECT) {
        powerupButton= [CCMenuItemFont itemWithString:@"power" target:self selector:@selector(collectAllOnScreen)];
    } else if (gameMode == GM_SURVIVE) {
        powerupButton= [CCMenuItemFont itemWithString:@"power" target:self selector:@selector(explodePowerUp)];
    }else {
        powerupButton= [CCMenuItemFont itemWithString:@"power" target:self selector:@selector(changeHeroClip)];
    }
    
    
    powerupButton.fontName = @"04b03";
    powerupButton.fontSize = 22;
    
    powerupButton.position = ccp(size.width-35, 15);
    
    powerupButton.color = _hudColor1;
    
    
    CCMenu *menu = [CCMenu menuWithItems:powerupButton, nil];
    
    menu.position = ccp(0,0);
    
    [self addChild:menu z:2000];
    
    
}

//
//create sprite with custom grid texture with custom color and size
//
-(CCSprite *)spriteWithColor:(ccColor3B)useColor textureSize:(CGSize)textureSize {
    
    int spriteSteviloPoY = ceil(textureSize.height/pixelSize);
    int spriteSteviloPoX = ceil(textureSize.width/pixelSize);
    
    // 1: Create new CCRenderTexture
    CCRenderTexture *rt = [CCRenderTexture renderTextureWithWidth:textureSize.width height:textureSize.height];
    
    // 2: Call CCRenderTexture:begin
    [rt beginWithClear:useColor.r/255.0f g:useColor.g/255.0f b:useColor.b/255.0f a:1.0f];
    
    // 3: Draw into the texture
    
    
    if (_testingMode == false) { // do proper background
        
        //draw squares with adjusted _bgColor1 and edge
        
        int edgeDiff = 20;
        
        //rows on Y axis
        for (int y=0; y<spriteSteviloPoY; y++) {
            
            //cell in column X and row Y
            
            for (int x=0; x<spriteSteviloPoX; x++) {
                
                int odmikRandomConst = [self doRandomFrom:-odmikBarveBG To:odmikBarveBG];
                
                
                int gradientShade = (ceil(spriteSteviloPoY/2)-y)*gradMult;
                
                
                int rdeca = 0;
                int zelena = 0;
                int modra = 0;
                
                
                
                
                rdeca = clampf((useColor.r+odmikRandomConst+gradientShade), 0, 255);
                zelena = clampf((useColor.g+odmikRandomConst+gradientShade), 0, 255);
                modra = clampf((useColor.b+odmikRandomConst+gradientShade), 0, 255);
                
                int scaleFactorForRetina = CC_CONTENT_SCALE_FACTOR();
                
                
                ccColor3B randomColor = ccc3(rdeca,zelena,modra);
                
                CCSprite *kvadratekBG = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekBG.textureRect = CGRectMake(0,0,pixelSize,pixelSize);
                kvadratekBG.position = ccp((x*pixelSize+pixelSize/2),(y*pixelSize+pixelSize/2));
                kvadratekBG.color = randomColor;
                kvadratekBG.opacity = 255;
                
                
                
                ccColor3B brightEdgeColor = ccc3(MIN(randomColor.r+edgeDiff,255),MIN(randomColor.g+edgeDiff,255),MIN(randomColor.b+edgeDiff,255));
                ccColor3B darkEdgeColor = ccc3(MAX(randomColor.r-edgeDiff,0),MAX(randomColor.g-edgeDiff,0),MAX(randomColor.b-edgeDiff,0));
                
                //levi rob
                CCSprite *kvadratekBrightRobLevo = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekBrightRobLevo.textureRect = CGRectMake(0,0,(1.0/scaleFactorForRetina),pixelSize);
                kvadratekBrightRobLevo.position = ccp((0.5/scaleFactorForRetina),pixelSize/2);
                kvadratekBrightRobLevo.color = brightEdgeColor;
                
                
                [kvadratekBG addChild:kvadratekBrightRobLevo];
                
                //zgornji rob
                CCSprite *kvadratekBrightRobZgoraj = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekBrightRobZgoraj.textureRect = CGRectMake(0,0,pixelSize,(1.0/scaleFactorForRetina));
                kvadratekBrightRobZgoraj.position = ccp(pixelSize/2,(0.5/scaleFactorForRetina));
                kvadratekBrightRobZgoraj.color = brightEdgeColor;
                
                [kvadratekBG addChild:kvadratekBrightRobZgoraj];
                
                
                //desni rob
                CCSprite *kvadratekDarkRobDesno = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekDarkRobDesno.textureRect = CGRectMake(0,0,(1.0/scaleFactorForRetina),pixelSize);
                kvadratekDarkRobDesno.position = ccp((pixelSize-(0.5/scaleFactorForRetina)),pixelSize/2);
                kvadratekDarkRobDesno.color = darkEdgeColor;
                
                
                [kvadratekBG addChild:kvadratekDarkRobDesno];
                
                //spodnji rob
                CCSprite *kvadratekDarkRobSpodaj = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekDarkRobSpodaj.textureRect = CGRectMake(0,0,pixelSize,(1.0/scaleFactorForRetina));
                kvadratekDarkRobSpodaj.position = ccp(pixelSize/2,(pixelSize-(0.5/scaleFactorForRetina)));
                kvadratekDarkRobSpodaj.color = darkEdgeColor;
                
                [kvadratekBG addChild:kvadratekDarkRobSpodaj];
                
                
                [kvadratekBG visit];
                
            }
            
        }
        
    } else { //test graphics
        
        NSLog(@"testing mode");
        
        int gridOdmik = 5;
        
        for (int y=0; y<steviloPoY; y++) {
            
            //doloci celico v stoplcu po X osi za vrstico Y
            
            for (int x=0; x<steviloPoX; x++) {
                
                NSLog(@"x %d, y %d",x+1, y+1);
                
                ccColor3B randomColor = useColor;
                
                if ((y+1) & 1)  { //y+1 je odd
                    
                    NSLog(@"odd -> y %d", y+1);
                    
                    if ((x+1) & 1)  {
                        NSLog(@"odd -> x %d", x+1);
                        
                        randomColor = ccc3(MAX((useColor.r-gridOdmik), 0), MAX((useColor.g-gridOdmik), 0), MAX((useColor.b-gridOdmik), 0));
                    }
                    
                    
                    
                } else { //y+1 je even
                    
                    NSLog(@"even -> y %d", y+1);
                    
                    if (((x+1) % 2) == 0)  {
                        NSLog(@"even -> x %d", x+1);
                        
                        randomColor = ccc3(MAX((useColor.r-gridOdmik), 0), MAX((useColor.g-gridOdmik), 0), MAX((useColor.b-gridOdmik), 0));
                    }
                    
                    
                }
                
                
                
                CCSprite *kvadratekBG = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekBG.textureRect = CGRectMake(0,0,pixelSize,pixelSize);
                kvadratekBG.position = ccp((x*pixelSize+pixelSize/2),(y*pixelSize+pixelSize/2));
                kvadratekBG.color = randomColor;
                kvadratekBG.opacity = 255;
                
                [kvadratekBG visit];
                
                
            }
        }
        
        
    }
    
    
    // 4: Call CCRenderTexture:end
    [rt end];
    
    // 5: Create a new Sprite from the texture
    return [CCSprite spriteWithTexture:rt.sprite.texture];
    
}

//
//generate background with grid
//
- (void)genBackground {
    
    CGSize newWinSize = winSize;
    
    newWinSize.width = ceil(winSize.width/pixelSize+1)*pixelSize;
    newWinSize.height = ceil(winSize.height/pixelSize)*pixelSize;
    
    [_background removeFromParentAndCleanup:YES];
    
    
    float darknessFactor = 0.85f;
    
    ccColor3B darkerBgColor = ccc3(_bgColor1.r*darknessFactor, _bgColor1.g*darknessFactor, _bgColor1.b*darknessFactor);
    
    _background = [self spriteWithColor:darkerBgColor textureSize:newWinSize];
    
    
    _background.textureRect = CGRectMake(0, 0, newWinSize.width, newWinSize.height);
    _background.anchorPoint = ccp(0.5,0.5);
    _background.position = ccp(winSize.width/2,winSize.height/2);
    
    
    [self addChild:_background z:1];
    
    
}

//
//game area background with grid texture and borders (fits in iphone4 screen)
//TO DO: iphone 5!
- (void)genGameLayerBackground {
    
    
    CGSize layerSize = CGSizeMake(480, 320);
    
    [_backgroundGame removeFromParentAndCleanup:YES];
    
    _backgroundGame = [self spriteWithColor:_bgColor1 textureSize:layerSize];
    
    
    _backgroundGame.textureRect = CGRectMake(0, 0, steviloPoX*pixelSize, steviloPoY*pixelSize);
    _backgroundGame.anchorPoint = ccp(0,0);
    _backgroundGame.position = ccp(0,0);
    
    [mainGameLayer addChild:_backgroundGame z:1];
    
    ccColor3B brightEdgeColor = ccc3(MIN(_bgColor1.r+20,255),MIN(_bgColor1.g+20,255),MIN(_bgColor1.b+20,255));
    ccColor3B darkEdgeColor = ccc3(MAX(_bgColor1.r-20,0),MAX(_bgColor1.g-20,0),MAX(_bgColor1.b-20,0));
    
    CCSprite *borderTop = [CCSprite spriteWithFile:@"whitepixel.png"];
    borderTop.textureRect = CGRectMake(0, 0, layerSize.width, 2);
    borderTop.color = brightEdgeColor;
    borderTop.anchorPoint = ccp(0,1);
    borderTop.position = ccp(0,layerSize.height);
    
    [_backgroundGame addChild:borderTop];
    
    CCSprite *borderLeft = [CCSprite spriteWithFile:@"whitepixel.png"];
    borderLeft.textureRect = CGRectMake(0, 0, 2, layerSize.height);
    borderLeft.color = brightEdgeColor;
    borderLeft.anchorPoint = ccp(0,0);
    borderLeft.position = ccp(0,0);
    
    [_backgroundGame addChild:borderLeft];
    
    CCSprite *borderBottom = [CCSprite spriteWithFile:@"whitepixel.png"];
    borderBottom.textureRect = CGRectMake(0, 0, layerSize.width, 2);
    borderBottom.color = darkEdgeColor;
    borderBottom.anchorPoint = ccp(0,0);
    borderBottom.position = ccp(0,0);
    
    [_backgroundGame addChild:borderBottom];
    
    CCSprite *borderRight = [CCSprite spriteWithFile:@"whitepixel.png"];
    borderRight.textureRect = CGRectMake(0, 0, 2, layerSize.height);
    borderRight.color = darkEdgeColor;
    borderRight.anchorPoint = ccp(1,0);
    borderRight.position = ccp(layerSize.width,0);
    
    [_backgroundGame addChild:borderRight];
    
    
    
    
    
}



-(void) makePlayableArea {
    
    CCSprite* playableAreaSprite = [CCSprite spriteWithFile:@"whitepixel.png"];
    playableAreaSprite.textureRect = CGRectMake(0, 0, 480, pixelSize);
    playableAreaSprite.color = _bgColor1;
    playableAreaSprite.position = ccp(240, 3*pixelSize+pixelSize/2);
    
    playableAreaSprite.opacity = 128;
    
    [mainGameLayer addChild:playableAreaSprite z:500];
    
}



//function that returs random value
//used for spawning objects and random direction of the enemy

-(int)doRandomFrom:(int)minVal To:(int)maxVal {
    
    int randomValue = (arc4random() % (maxVal - minVal+1) + minVal);
    
    return randomValue;
    
}

-(float)doRandomFloat:(float)minVal To:(float)maxVal {
    
    float range = maxVal - minVal;
    float val = ((float)arc4random() / ARC4RANDOM_MAX) * range + minVal;
    return val;
}

//
// GENERATE TEXTURE FOR STATIC COLLECTIBLES
// create texture with a grid of squares
// each square color deviates from the supplied useColor for a certain amount
// draw border shading on square (1 pixel wide edge, lighter on left/top, darker on right/bottom)
//
-(CCRenderTexture* ) genStaticTextureWithColor:(ccColor3B)useColor textureSize:(int)textureSizeBorder{
    
    // 1: Create new CCRenderTexture
    CCRenderTexture *rtx = [CCRenderTexture renderTextureWithWidth:textureSizeBorder height:textureSizeBorder];
    
    // 2: Call CCRenderTexture:begin
    [rtx beginWithClear:useColor.r/255.0f g:useColor.g/255.0f b:useColor.b/255.0f a:1.0f];
    
    // 3: Draw into the texture
    
    if (_testingMode == false) {
        
        int textureSize = textureSizeBorder-2;
        
        int zamik = (textureSizeBorder-textureSize)/2; //calculate indent
        //base square+border
        
        CCSprite *baseKvadratek = [CCSprite spriteWithFile:@"whitepixel.png"];
        baseKvadratek.textureRect = CGRectMake(0,0,textureSizeBorder,textureSizeBorder);
        baseKvadratek.position = ccp((textureSizeBorder/2),(textureSizeBorder/2));
        baseKvadratek.color = useColor;
        //baseKvadratek.color = _hudColor1;
        baseKvadratek.opacity = 255;
        
        [baseKvadratek visit];
        
        
        int steviloPoX = 4/(pixelSize/textureSize);
        int steviloPoY = 4/(pixelSize/textureSize);
        
        int elementSize = textureSize/steviloPoX;
        
        //draw squares with random color from _useColor with shaded edge
        
        //row Y
        for (int y=0; y<steviloPoY; y++) {
            
            //column X
            
            for (int x=0; x<steviloPoX; x++) {
                
                int odmikRandomConst = [self doRandomFrom:-odmikBarve*3 To:odmikBarve*2];
                
                int gradientShade = (ceil(steviloPoY/2)-y)*gradMult;
                
                int rdeca = 0;
                int zelena = 0;
                int modra = 0;
                
                rdeca = clampf((useColor.r+odmikRandomConst +gradientShade), 0, 255);
                zelena = clampf((useColor.g+odmikRandomConst +gradientShade), 0, 255);
                modra = clampf((useColor.b+odmikRandomConst +gradientShade), 0, 255);
                
                ccColor3B randomColor = ccc3(rdeca,zelena,modra);
                
                CCSprite *kvadratek = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratek.textureRect = CGRectMake(0,0,elementSize,elementSize);
                kvadratek.position = ccp((x*elementSize+elementSize/2)+zamik,(y*elementSize+elementSize/2)+zamik);
                kvadratek.color = randomColor;
                kvadratek.opacity = 255;
                
                
                
                int scaleFactorForRetina = CC_CONTENT_SCALE_FACTOR(); //scale factor for retina to keep edge always 1 pixel wide
                
                ccColor3B brightEdgeColor = ccc3(MIN(randomColor.r+20,255),MIN(randomColor.g+20,255),MIN(randomColor.b+20,255));
                ccColor3B darkEdgeColor = ccc3(MAX(randomColor.r-20,0),MAX(randomColor.g-20,0),MAX(randomColor.b-20,0));
                
                //left edge
                CCSprite *kvadratekBrightRobLevo = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekBrightRobLevo.textureRect = CGRectMake(0,0,(1.0/scaleFactorForRetina),elementSize);
                kvadratekBrightRobLevo.position = ccp((0.5/scaleFactorForRetina),elementSize/2);
                kvadratekBrightRobLevo.color = brightEdgeColor;
                
                
                [kvadratek addChild:kvadratekBrightRobLevo];
                
                //upper edge
                CCSprite *kvadratekBrightRobZgoraj = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekBrightRobZgoraj.textureRect = CGRectMake(0,0,elementSize,(1.0/scaleFactorForRetina));
                kvadratekBrightRobZgoraj.position = ccp(elementSize/2,(0.5/scaleFactorForRetina));
                kvadratekBrightRobZgoraj.color = brightEdgeColor;
                
                [kvadratek addChild:kvadratekBrightRobZgoraj];
                
                
                //right edge
                CCSprite *kvadratekDarkRobDesno = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekDarkRobDesno.textureRect = CGRectMake(0,0,(1.0/scaleFactorForRetina),elementSize);
                kvadratekDarkRobDesno.position = ccp((elementSize-(0.5/scaleFactorForRetina)),elementSize/2);
                kvadratekDarkRobDesno.color = darkEdgeColor;
                
                
                [kvadratek addChild:kvadratekDarkRobDesno];
                
                //bottom edge
                CCSprite *kvadratekDarkRobSpodaj = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekDarkRobSpodaj.textureRect = CGRectMake(0,0,elementSize,(1.0/scaleFactorForRetina));
                kvadratekDarkRobSpodaj.position = ccp(elementSize/2,(elementSize-(0.5/scaleFactorForRetina)));
                kvadratekDarkRobSpodaj.color = darkEdgeColor;
                
                [kvadratek addChild:kvadratekDarkRobSpodaj];
                
                [kvadratek visit];
                //
                
            }
            
        }
        
    }
    
    
    // 4: Call CCRenderTexture:end
    [rtx end];
    
    return rtx;
    
}


//
// GENERATE TEXTURE FOR MOVING OBJECTS
// create texture with a grid of squares
// each square color deviates from the supplied useColor for a certain amount
// store color for each square in array for each object
// change order from array based on direction
// draw border shading on square (1 pixel wide edge, lighter on left/top, darker on right/bottom)
//
-(CCRenderTexture* ) genTextureWithColor:(ccColor3B)useColor textureSize:(int)textureSizeBorder sprite:(subSprite *)sprite direction:(int)direction{
    
    // 1: Create new CCRenderTexture
    CCRenderTexture *rtx = [CCRenderTexture renderTextureWithWidth:textureSizeBorder height:textureSizeBorder];
    
    
    
    // 2: Call CCRenderTexture:begin
    [rtx beginWithClear:useColor.r/255.0f g:useColor.g/255.0f b:useColor.b/255.0f a:1.0f];
    
    // 3: Draw into the texture
    
    
    
    if (_testingMode == false) {
        
        
        
        int textureSize = textureSizeBorder-2;
        
        int zamik = (textureSizeBorder-textureSize)/2;
        //base square+border
        
        CCSprite *baseKvadratek = [CCSprite spriteWithFile:@"whitepixel.png"];
        baseKvadratek.textureRect = CGRectMake(0,0,textureSizeBorder,textureSizeBorder);
        baseKvadratek.position = ccp((textureSizeBorder/2),(textureSizeBorder/2));
        baseKvadratek.color = useColor;
        baseKvadratek.opacity = 255;
        
        [baseKvadratek visit];
        
        
        
        int steviloPoXtex = 4/(pixelSize/textureSize);
        int steviloPoYtex = 4/(pixelSize/textureSize);
        
        int elementSize = textureSize/steviloPoXtex;
        
        //draw squares with random _bgColor1 with X deviation + shaded edge
        
        //get color NSarray
        
        NSMutableArray *colorArray;
        int indexNum = 0;
        
        if (sprite.colorArray != NULL) {
            
            colorArray = sprite.colorArray;
            
        } else {
            
            colorArray = [NSMutableArray new];
            
        }
        
        //rows on Y axis
        for (int y=0; y<steviloPoYtex; y++) {
            
            //cell in column X for row Y
            
            for (int x=0; x<steviloPoXtex; x++) {
                
                
                
                ccColor3B randomColorUse;
                
                if (colorArray.count < steviloPoXtex*steviloPoYtex) {
                    
                    int odmikRandomConst = [self doRandomFrom:-odmikBarve To:odmikBarve];
                    
                    
                    int rdeca = 0;
                    int zelena = 0;
                    int modra = 0;
                    rdeca = clampf((useColor.r+odmikRandomConst), 0, 255);
                    zelena = clampf((useColor.g+odmikRandomConst), 0, 255);
                    modra = clampf((useColor.b+odmikRandomConst), 0, 255);
                    
                    
                    if ((y==0 && x==0) || (y==0 && x==3) || (y==2 && x==1) || (y==2 && x==2)) {
                        rdeca = rdeca*0.80;
                        zelena = zelena*0.80;
                        modra = modra*0.80;
                        
                        
                        if (rdeca < 0)
                            rdeca = 0;
                        
                        if (zelena < 0)
                            zelena = 0;
                        
                        if (modra < 0)
                            modra = 0;
                        
                    } else if ((y==3 && x==1) || (y==3 && x==2)) {
                        
                        rdeca = rdeca*0.5;
                        zelena = zelena*0.5;
                        modra = modra*0.5;
                        
                        
                        if (rdeca < 0)
                            rdeca = 0;
                        
                        if (zelena < 0)
                            zelena = 0;
                        
                        if (modra < 0)
                            modra = 0;
                        
                        
                    }
                    
                    ccColor3B randomColor = ccc3(rdeca,zelena,modra);
                    
                    
                    [colorArray addObject:[NSValue valueWithBytes:&randomColor objCType:@encode(ccColor3B)]];
                    
                    randomColorUse = randomColor;
                    
                } else {
                    
                    
                    //determine indexNumber of cell to match the appropriate color from the array, based on direction
                    
                    switch (direction) {
                        case 0: //no direction
                            
                            indexNum = (y*steviloPoXtex)+x;
                            
                            break;
                            
                        case 1: //up
                            
                            indexNum = (y*steviloPoXtex)+x;
                            
                            break;
                            
                        case 2: // down
                            
                            indexNum = ((steviloPoXtex*steviloPoYtex)-1) - ((y*steviloPoXtex)+x);
                            
                            break;
                            
                        case 3: // right
                            
                            indexNum = (((steviloPoXtex-1)*(steviloPoXtex))+y) - (x*steviloPoXtex);
                            
                            break;
                            
                        case 4: // left
                            
                            indexNum = ((x*steviloPoXtex) + ((steviloPoYtex-1)-y));
                            
                            break;
                            
                        default:
                            break;
                    }
                    
                    
                    ccColor3B currentColor;
                    
                    [[colorArray objectAtIndex:indexNum] getValue:&currentColor];
                    
                    
                    randomColorUse = currentColor;
                    
                }
                
                
                //DRAW GRADIENT ON SQUARES
                
                int gradientShade = (ceil(steviloPoY/2)-y)*gradMult*2;
                
                int rdecaGrad = 0;
                int zelenaGrad = 0;
                int modraGrad = 0;
                rdecaGrad = clampf((randomColorUse.r+gradientShade), 0, 255);
                zelenaGrad = clampf((randomColorUse.g+gradientShade), 0, 255);
                modraGrad = clampf((randomColorUse.b+gradientShade), 0, 255);
                
                
                randomColorUse = ccc3(rdecaGrad, zelenaGrad, modraGrad);
                
                int scaleFactorForRetina = CC_CONTENT_SCALE_FACTOR(); //if retina scale width of edges
                CCSprite *kvadratek = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratek.textureRect = CGRectMake(0,0,elementSize,elementSize);
                kvadratek.position = ccp((x*elementSize+elementSize/2)+zamik,(y*elementSize+elementSize/2)+zamik);
                kvadratek.color = randomColorUse;
                kvadratek.opacity = 255;
                
                
                ccColor3B brightEdgeColor = ccc3(MIN(randomColorUse.r+20,255),MIN(randomColorUse.g+20,255),MIN(randomColorUse.b+20,255));
                ccColor3B darkEdgeColor = ccc3(MAX(randomColorUse.r-20,0),MAX(randomColorUse.g-20,0),MAX(randomColorUse.b-20,0));
                
                //left edge
                CCSprite *kvadratekBrightRobLevo = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekBrightRobLevo.textureRect = CGRectMake(0,0,(1.0/scaleFactorForRetina),elementSize);
                kvadratekBrightRobLevo.position = ccp((0.5/scaleFactorForRetina),elementSize/2);
                kvadratekBrightRobLevo.color = brightEdgeColor;
                
                
                [kvadratek addChild:kvadratekBrightRobLevo];
                
                //upper edge
                CCSprite *kvadratekBrightRobZgoraj = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekBrightRobZgoraj.textureRect = CGRectMake(0,0,elementSize,(1.0/scaleFactorForRetina));
                kvadratekBrightRobZgoraj.position = ccp(elementSize/2,(0.5/scaleFactorForRetina));
                kvadratekBrightRobZgoraj.color = brightEdgeColor;
                
                [kvadratek addChild:kvadratekBrightRobZgoraj];
                
                
                //right edge
                CCSprite *kvadratekDarkRobDesno = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekDarkRobDesno.textureRect = CGRectMake(0,0,(1.0/scaleFactorForRetina),elementSize);
                kvadratekDarkRobDesno.position = ccp((elementSize-(0.5/scaleFactorForRetina)),elementSize/2);
                kvadratekDarkRobDesno.color = darkEdgeColor;
                
                
                [kvadratek addChild:kvadratekDarkRobDesno];
                
                //bottom edge
                CCSprite *kvadratekDarkRobSpodaj = [CCSprite spriteWithFile:@"whitepixel.png"];
                kvadratekDarkRobSpodaj.textureRect = CGRectMake(0,0,elementSize,(1.0/scaleFactorForRetina));
                kvadratekDarkRobSpodaj.position = ccp(elementSize/2,(elementSize-(0.5/scaleFactorForRetina)));
                kvadratekDarkRobSpodaj.color = darkEdgeColor;
                
                [kvadratek addChild:kvadratekDarkRobSpodaj];
                
                [kvadratek visit];
                //
                
            }
            
        }
        
        //store colors in array
        sprite.colorArray = colorArray;
        
    }
    
    
    // 4: Call CCRenderTexture:end
    [rtx end];
    
    return rtx;
    
}

//
// SPAWN HERO CHARACTER
//

-(void) spawnSelf {
    
    movingSquare = [subSprite spriteWithTexture:[self genTextureWithColor:_heroColor textureSize:pixelSize+2 sprite:movingSquare direction:0].sprite.texture];
    
    int spawnCoordinateX = floor(steviloPoX/2)+2;
    
    if (gameMode == GM_COLLECT) {
        spawnCoordinateX = spawnCoordinateX-2;
    } else if (gameMode == GM_CLASSIC) {
        spawnCoordinateX = spawnCoordinateX-1;
    }
    
    int spawnCoordinateY = floor(steviloPoY/2);
    
    
    if (gh == true) { // if gh mode true, limit hero movement to one line on grid
        spawnCoordinateY = 3;
        [self makePlayableArea];
    }
    
    movingSquare.position = ccp( ((spawnCoordinateX*pixelSize)+pixelSize/2), ((spawnCoordinateY*pixelSize)+pixelSize/2) );
    
    movingSquare.tag = 1;
    
    [mainGameLayer addChild:movingSquare z:1000];
    
    
}

//
// SPAWN (dumb) ENEMY CHARACTER
// "dumb" enemy moves around randomly - or doesn't move at all
//
-(void) spawnEnemy {
    
    
    
    subSprite *enemySprite;
    
    enemySprite = [subSprite spriteWithTexture:[self genTextureWithColor:_enemyColor textureSize:pixelSize+2 sprite:enemySprite direction:0].sprite.texture];
    
    
    enemySprite.opacity = 150;
    
    int spawnCoordinateX = floor(steviloPoX/2)-2;
    
    int spawnCoordinateY = floor(steviloPoY/2);
    
    enemySprite.position = ccp( ((spawnCoordinateX*pixelSize)+pixelSize/2), ((spawnCoordinateY*pixelSize)+pixelSize/2) );
    
    enemySprite.tag = 35;
    
    
    [mainGameLayer addChild:enemySprite z:1000];
    
}

//
// SPAWN FOLLOW (smart) ENEMY CHARACTER
// "smart" enemy always moves towards HERO
// chooses the shortest way (if clipping disabled, also goes through walls)
// speeds up on certain intervals
//
-(void) spawnFollowEnemy:(CGPoint)p {
    
    subSprite *enemySprite;
    
    enemySprite = [subSprite spriteWithTexture:[self genTextureWithColor:_enemyColor textureSize:pixelSize+2 sprite:enemySprite direction:0].sprite.texture];
    
    
    enemySprite.position = p; //12,8
    
    enemySprite.tag = 36;
    
    [mainGameLayer addChild:enemySprite z:1000];
    
    [self followSpriteSpawnAnimation:enemySprite];
    
}

//
// SPAWN COLLECTIBLE ITEM
// choose random type of collectible (for combos and powerups)
// type determines color
// if gh mode, collectible spawns out of bounds, and has a certain speed, falling downwards
// else collectibles spawn randomly around grid
//
-(void) spawnCollectible {
    
    //choose type of collectible 1-3
    
    int collectibleTypeRandom = [self doRandomFrom:1 To:3];
    
    float rotationValue = 0.0;
    
    
    subSprite *collectibleSprite;
    
    ccColor3B colorChoose;
    
    
    //temporary assign different colors to different types of consumables/collectibles
    switch (collectibleTypeRandom) {
        case 1:
            
            colorChoose = ccc3(_consumableColor.r, _consumableColor.g, _consumableColor.b);
            rotationValue = 0; //30.0;
            
            break;
            
        case 2:
            
            colorChoose = ccc3(_consumableColor.g, _consumableColor.b, _consumableColor.r);
            rotationValue = 0; //-30.0;
            
            break;
            
        case 3:
            
            colorChoose = ccc3(_consumableColor.b, _consumableColor.r, _consumableColor.g);
            rotationValue = 0.0;
            
            break;
            
        default:
            break;
    }
    
    
    collectibleSprite = [subSprite spriteWithTexture:[self genStaticTextureWithColor:colorChoose textureSize:(pixelSize/2+2)].sprite.texture];
    
    
    
    //store collectible default color
    collectibleSprite.savedColor = colorChoose;
    
    ///////
    //encode type of collectible 1-3
    collectibleSprite.collectibleType = collectibleTypeRandom;
    
    int randomGridTileX = [self doRandomFrom:1 To:steviloPoX];
    
    
    if (gh != true) {
        
        int randomGridTileY = [self doRandomFrom:1 To:steviloPoY];
        
        collectibleSprite.position = ccp( ((randomGridTileX*pixelSize)-pixelSize/2), ((randomGridTileY*pixelSize)-pixelSize/2) );
        
    } else {
        //distribute collectibles at the top with random position X
        collectibleSprite.position = ccp( ((randomGridTileX*pixelSize)-pixelSize/2), ((steviloPoY+3)*pixelSize)+pixelSize/2);
        
        collectibleSprite.subSpriteTime = 0;
        collectibleSprite.subSpriteSpeed = [self doRandomFrom:15 To:25];
    }
    
    
    collectibleSprite.tag = 60;
    
    
    [mainGameLayer addChild:collectibleSprite z:999];
    
    if (gh != true) {
        
        //DESTROY COLLECTIBLE SPRITES WITHIN TIME RANDOM
        float randomTime = [self doRandomFloat:2.0f To:4.0f];
        
        id timeDelay = [CCDelayTime actionWithDuration:randomTime];
        
        id destroySprite = [CCCallFuncN actionWithTarget:self selector:@selector(autoDestroyCollectible:)];
        
        id sequenceDestroy = [CCSequence actions:timeDelay, destroySprite, nil];
        
        [collectibleSprite runAction:[CCSpawn actions: sequenceDestroy, nil]];
        
    }
}


//
// DESTROY OBJECTS
// remove from play
//
-(void) destroyCollectible:(subSprite*)sprite {
    
    sprite.tag = 111;
    
    sprite.position = ccp(-2000,-2000); //remove from screen before scheduler removes it
    
    sprite.visible = false;
    
    
    
    if ([sprite numberOfRunningActions] != 0) {
        [sprite stopAllActions];
    }
    
    [sprite removeFromParentAndCleanup:YES];
    
    sprite = NULL;
    
}

-(void) autoDestroyCollectible:(id)sender {
    
    subSprite *sprite = (subSprite*)sender;
    
    [self destroyCollectible:sprite];
    
}




bool accelUpdated = NO;

float ayx = -.63f;
float ayy = 0;
float ayz = -.92f;

float abcx = 0.0f;
float abcy = 0.0f;
float abcz = 0.0f;


//accelerometor at the start of the game
- (void) accelObStartu {
    
    if (gameOver == NO && isStart == YES) {
        
        ayx = abcx;
        ayy = 0.0f;//abcy;
        ayz = abcz;
        accelUpdated = YES;
        NSLog(@"%f,%f,%f",ayx, ayy, ayz);
        
    }
    
    
}


int updateCount = 0;
int updateFollowCount = 0;

int timerCount = 0;

int collectibleTimerCount = 0;
int spawnCollectibleTime = minTime;

int updateEnemyOnCount = 48;
int updateFollowEnemyOnCount = 48;

int moveUpdateCounter = 0;

//game update loop
-(void) update: (ccTime) dt
{
    
    
    
    if (gameOver == NO){
        
        if (isStart == YES) {
            
            if (timerCount == 0 && gameMode == GM_SURVIVE) {
                
                int spawnCoordinateX = floor(steviloPoX/2)-2;
                
                int spawnCoordinateY = floor(steviloPoY/2);
                
                CGPoint followEnemySpawnPos = ccp( ((spawnCoordinateX*pixelSize)+pixelSize/2), ((spawnCoordinateY*pixelSize)+pixelSize/2) );
                
                [self spawnFollowEnemy:followEnemySpawnPos];
            }
            
            CCArray *loopArray = [[mainGameLayer children] copy];
            
            
            if(gameMode == GM_CLASSIC) {
                
                switch (timerCount) {
                        
                    case 0:
                        updateEnemyOnCount = 24;
                        updateCount = 0;
                        break;
                        
                    case 300:
                        updateEnemyOnCount = 18;
                        updateCount = 0;
                        break;
                        
                    case 600:
                        updateEnemyOnCount = 12;
                        updateCount = 0;
                        break;
                        
                    case 900:
                        
                        for (subSprite *sprite in loopArray) {
                            
                            if (sprite.tag == 35) { //if dumb enemy
                                [self spawnFollowEnemy:sprite.position];
                                [sprite removeFromParentAndCleanup:YES];
                            }
                            
                        }
                        
                        updateFollowEnemyOnCount = 36;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    case 1500:
                        updateFollowEnemyOnCount = 30;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    case 2100:
                        updateFollowEnemyOnCount = 24;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    case 3000:
                        updateFollowEnemyOnCount = 18;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    case 4000:
                        updateFollowEnemyOnCount = 12;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    case 5000:
                        updateFollowEnemyOnCount = 6;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    default:
                        break;
                }
                
            } else if(gameMode == GM_SURVIVE) {
                
                switch (timerCount) {
                        
                    case 300:
                        
                        updateFollowEnemyOnCount = 36;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    case 600:
                        updateFollowEnemyOnCount = 30;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    case 900:
                        updateFollowEnemyOnCount = 24;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    case 1500:
                        updateFollowEnemyOnCount = 18;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    case 2100:
                        updateFollowEnemyOnCount = 12;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    case 3000:
                        updateFollowEnemyOnCount = 6;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    case 4000:
                        updateFollowEnemyOnCount = 3;
                        updateFollowCount = 0;
                        NSLog(@"speed up %d", updateFollowEnemyOnCount);
                        break;
                        
                    default:
                        break;
                }
            }
            
            
            
            if (gameMode == GM_CLASSIC) {
                
                if (updateCount == updateEnemyOnCount) {
                    
                    //NSLog(@"update %d", updateCount);
                    
                    updateCount = 0;
                    
                    
                    for (subSprite *sprite in loopArray) {
                        
                        if (sprite.tag == 35) { //enemy tag = 35
                            
                            [self moveEnemy:sprite];
                            
                        }
                        
                    }
                    
                } else {
                    
                    updateCount++;
                }
                
            }
            
            
            if (gameMode == GM_SURVIVE || gameMode == GM_CLASSIC) {
                
                
                if (updateFollowCount == updateFollowEnemyOnCount) {
                    
                    updateFollowCount = 0;
                    
                    
                    for (subSprite *sprite in loopArray) {
                        
                        if (sprite.tag == 36) { //follow enemy - tag = 36
                            
                            [self moveFollowEnemy:sprite];
                            
                        }
                    }
                    
                } else {
                    
                    updateFollowCount++;
                }
                
            }
            
            
            if (gh == true) {
                [self checkCollectibleMovement];
            }
            
            
            [self checkHeroMovement];
            [self updateTrailSpriteBlocks];
            
            //[self checkClippingPath];
            
            [self checkCollisions];
            [self startTimer];
            
            
            
            if (gameMode != GM_SURVIVE) {
                [self timeCollectibles];
            }
            
            timerCount++;
            
        } else {
            
            [self countdownToStart];
            
        }
        
    }
    
}


#pragma mark animated collectible GH mode

-(void) checkCollectibleMovement {
    
    [self incomingCollectibleIndicator];
    
    CCArray *loopArray = [[mainGameLayer children] copy];
    
    
    
    for (id obj in loopArray) {
        
        if ([obj isMemberOfClass:[subSprite class]]) {
            
            
            subSprite *sprite = (subSprite*)obj;
            
            //NSLog(@"sprite with tag %d and description %@", sprite.tag, sprite.description);
            if (sprite != NULL && sprite.visible != NO) {
                
                if (sprite.tag == 60) {
                    
                    //DO COLLECTIBLE MOVEMENT VERTICAL by subSpriteSpeed data
                    
                    
                    
                    if (sprite.subSpriteTime == sprite.subSpriteSpeed) {
                        
                        sprite.subSpriteTime = 0;
                        
                        [self moveCollectible:sprite];
                        
                        
                    } else {
                        
                        sprite.subSpriteTime++;
                    }
                    
                }
            }
        }
    }
    
}

//
// DISPLAY INDICATOR ON TOP OF SCREEN FOR NEXT INCOMING COLLECTIBLE
// loop through all collectibles that are not in view
// draw indicator in the appropriate grid column
// indicator is the same color as the incoming collectible
//
-(void) incomingCollectibleIndicator {
    
    
    CCNode *aChild;
    
    while ((aChild = [mainGameLayer getChildByTag:445]) != nil) {
        [aChild removeFromParentAndCleanup:YES];
    }
    
    
    
    CCArray *loopArray = [[mainGameLayer children] copy];
    
    
    
    for (id obj in loopArray) {
        
        if ([obj isMemberOfClass:[subSprite class]]) {
            
            subSprite *sprite = (subSprite*)obj;
            
            if (sprite != NULL && sprite.visible != NO) {
                
                if (sprite.tag == 60) {
                    
                    //SHOW COLLECTIBLE INDICATOR
                    
                    if (sprite.position.y > 320-pixelSize/2) {
                        
                        
                        CCSprite *proximityIndicatorY2 = [CCSprite spriteWithFile:@"whitepixel.png"];
                        proximityIndicatorY2.textureRect = CGRectMake(0, 0, pixelSize, 2);
                        proximityIndicatorY2.position = ccp(sprite.position.x,319);
                        proximityIndicatorY2.color = sprite.savedColor;
                        proximityIndicatorY2.tag = 445;
                        
                        [mainGameLayer addChild:proximityIndicatorY2 z: 500];
                        
                        
                        CCSprite *proximityGradientY2 = [self clippingGradientSpriteWithColor:sprite.savedColor textureSize:pixelSize];
                        proximityGradientY2.position = ccp(sprite.position.x, 320-pixelSize/2);
                        proximityGradientY2.tag = 445;
                        
                        proximityGradientY2.rotation = 180;
                        proximityGradientY2.opacity = 80;
                        
                        [mainGameLayer addChild:proximityGradientY2 z:498];
                        
                        
                    }
                    
                    
                    
                }
            }
        }
    }
    
    
}


//
// animate falling collectible in gh mode
// leave trail behind collectible
// if collectible reaches bottom of screen, destroy it
//
-(void) moveCollectible:(subSprite*)sprite {
    
    
    if (sprite.position.y > 0) {
        
        [sprite setPosition:ccp(sprite.position.x, sprite.position.y-pixelSize)];
        
        //create trail after move
        [self leaveCollectibleTrail:sprite];
        
    } else {
        
        //went off screen
        [self destroyCollectible:sprite];
        
    }
    
}


int previousMoveState = MS_STOP;
int movementTimerKonst = 5;


//
// check if HERO movement timer is matched with the timer update constant
// execute movement function
// update movement timer
//
-(void) checkHeroMovement {
    
    if (moveUpdateCounter == movementTimerKonst) {
        
        [self doMove];
        
        moveUpdateCounter = 0;
    } else {
        moveUpdateCounter++;
    }
    
}

//
// IF HERO CLIPPING DISABLED, CHECK IF HERO WILL GO THROUGH WALL
// if hero is close to a wall, display proximity indicator and "portal" to corresponding wall
// portal/indicator is the same color as hero's main color
//
-(void)checkClippingHero {
    
    if (_testingMode == false) {
        
        
        //HERO CLIPPING INDICATOR
        
        if (_heroClip == false) {
            
            CCNode *aChild;
            
            while ((aChild = [mainGameLayer getChildByTag:345]) != nil) {
                [aChild removeFromParentAndCleanup:YES];
            }
            
            if ((movingSquare.position.x < 2*pixelSize) || (movingSquare.position.x > (480-2*pixelSize))) {
                
                CCSprite *proximityIndicatorX1 = [CCSprite spriteWithFile:@"whitepixel.png"];
                proximityIndicatorX1.textureRect = CGRectMake(0, 0, 2, pixelSize);
                proximityIndicatorX1.position = ccp(1,movingSquare.position.y);
                proximityIndicatorX1.color = _heroColor;
                proximityIndicatorX1.tag = 345;
                
                [mainGameLayer addChild:proximityIndicatorX1 z: 500];
                
                CCSprite *proximityIndicatorX2 = [CCSprite spriteWithFile:@"whitepixel.png"];
                proximityIndicatorX2.textureRect = CGRectMake(0, 0, 2, pixelSize);
                proximityIndicatorX2.position = ccp(479,movingSquare.position.y);
                proximityIndicatorX2.color = _heroColor;
                proximityIndicatorX2.tag = 345;
                
                [mainGameLayer addChild:proximityIndicatorX2 z: 500];
                
                
                //do gradient glow
                
                CCSprite *proximityGradientX1 = [self clippingGradientSpriteWithColor:_heroColor textureSize:pixelSize];
                proximityGradientX1.position = ccp(pixelSize/2,movingSquare.position.y);
                proximityGradientX1.tag = 345;
                
                proximityGradientX1.rotation = 90;
                proximityGradientX1.opacity = 40;
                
                [mainGameLayer addChild:proximityGradientX1 z:498];
                
                
                CCSprite *proximityGradientX2 = [self clippingGradientSpriteWithColor:_heroColor textureSize:pixelSize];
                proximityGradientX2.position = ccp(480-pixelSize/2,movingSquare.position.y);
                proximityGradientX2.tag = 345;
                
                proximityGradientX2.rotation = -90;
                proximityGradientX2.opacity = 40;
                
                [mainGameLayer addChild:proximityGradientX2 z:498];
                
                
                if ((movingSquare.position.x < pixelSize) || (movingSquare.position.x > (480-pixelSize))) {
                    
                    CCSprite *proximityIndicatorX1 = [CCSprite spriteWithFile:@"whitepixel.png"];
                    proximityIndicatorX1.textureRect = CGRectMake(0, 0, 2, 3*pixelSize);
                    proximityIndicatorX1.position = ccp(1,movingSquare.position.y);
                    proximityIndicatorX1.color = ccc3((_heroColor.r+_bgColor1.r)/2,(_heroColor.g+_bgColor1.g)/2,(_heroColor.b+_bgColor1.b)/2);
                    proximityIndicatorX1.tag = 345;
                    
                    [mainGameLayer addChild:proximityIndicatorX1 z: 499];
                    
                    CCSprite *proximityIndicatorX2 = [CCSprite spriteWithFile:@"whitepixel.png"];
                    proximityIndicatorX2.textureRect = CGRectMake(0, 0, 1, 3*pixelSize);
                    proximityIndicatorX2.position = ccp(479,movingSquare.position.y);
                    proximityIndicatorX2.color = ccc3((_heroColor.r+_bgColor1.r)/2,(_heroColor.g+_bgColor1.g)/2,(_heroColor.b+_bgColor1.b)/2);
                    proximityIndicatorX2.tag = 345;
                    
                    [mainGameLayer addChild:proximityIndicatorX2 z: 499];
                    
                    //
                    proximityGradientX1.opacity = 80;
                    proximityGradientX2.opacity = 80;
                    
                }
                
                
                
            }
            
            if ((movingSquare.position.y < 2*pixelSize) || (movingSquare.position.y > (320-2*pixelSize))) {
                //NSLog(@"hero near clip y");
                
                CCSprite *proximityIndicatorY1 = [CCSprite spriteWithFile:@"whitepixel.png"];
                proximityIndicatorY1.textureRect = CGRectMake(0, 0, pixelSize, 2);
                proximityIndicatorY1.position = ccp(movingSquare.position.x,1);
                proximityIndicatorY1.color = _heroColor;
                proximityIndicatorY1.tag = 345;
                
                [mainGameLayer addChild:proximityIndicatorY1 z: 500];
                
                CCSprite *proximityIndicatorY2 = [CCSprite spriteWithFile:@"whitepixel.png"];
                proximityIndicatorY2.textureRect = CGRectMake(0, 0, pixelSize, 2);
                proximityIndicatorY2.position = ccp(movingSquare.position.x,319);
                proximityIndicatorY2.color = _heroColor;
                proximityIndicatorY2.tag = 345;
                
                [mainGameLayer addChild:proximityIndicatorY2 z: 500];
                
                
                //do gradient glow
                
                CCSprite *proximityGradientY1 = [self clippingGradientSpriteWithColor:_heroColor textureSize:pixelSize];
                proximityGradientY1.position = ccp(movingSquare.position.x,pixelSize/2);
                proximityGradientY1.tag = 345;
                
                proximityGradientY1.rotation = 0;
                proximityGradientY1.opacity = 40;
                
                [mainGameLayer addChild:proximityGradientY1 z:498];
                
                
                CCSprite *proximityGradientY2 = [self clippingGradientSpriteWithColor:_heroColor textureSize:pixelSize];
                proximityGradientY2.position = ccp(movingSquare.position.x, 320-pixelSize/2);
                proximityGradientY2.tag = 345;
                
                proximityGradientY2.rotation = 180;
                proximityGradientY2.opacity = 40;
                
                [mainGameLayer addChild:proximityGradientY2 z:498];
                
                
                
                if ((movingSquare.position.y < pixelSize) || (movingSquare.position.y > (320-pixelSize))) {
                    
                    CCSprite *proximityIndicatorY1 = [CCSprite spriteWithFile:@"whitepixel.png"];
                    proximityIndicatorY1.textureRect = CGRectMake(0, 0, pixelSize*3, 2);
                    proximityIndicatorY1.position = ccp(movingSquare.position.x,1);
                    proximityIndicatorY1.color = ccc3((_heroColor.r+_bgColor1.r)/2,(_heroColor.g+_bgColor1.g)/2,(_heroColor.b+_bgColor1.b)/2);
                    proximityIndicatorY1.tag = 345;
                    
                    [mainGameLayer addChild:proximityIndicatorY1 z: 499];
                    
                    CCSprite *proximityIndicatorY2 = [CCSprite spriteWithFile:@"whitepixel.png"];
                    proximityIndicatorY2.textureRect = CGRectMake(0, 0, pixelSize*3, 2);
                    proximityIndicatorY2.position = ccp(movingSquare.position.x,319);
                    proximityIndicatorY2.color = ccc3((_heroColor.r+_bgColor1.r)/2,(_heroColor.g+_bgColor1.g)/2,(_heroColor.b+_bgColor1.b)/2);
                    proximityIndicatorY2.tag = 345;
                    
                    [mainGameLayer addChild:proximityIndicatorY2 z: 499];
                    
                    
                    //
                    proximityGradientY1.opacity = 80;
                    proximityGradientY2.opacity = 80;
                    
                }
                
            }
            
        }
        
        
    }
    
}

//
// IF ENEMY CLIPPING DISABLED, CHECK IF ENEMY WILL GO THROUGH WALL
// if enemy is close to a wall, display proximity indicator and "portal" to corresponding wall
// portal/indicator is the same color as enemy's main color
//
-(void)checkClippingEnemy:(subSprite*)sprite {
    
    if (_testingMode == false) {
        
        
        //ENEMY CLIPPING INDICATOR
        
        if (_enemyClip == false) {
            
            CCNode *aChild;
            
            while ((aChild = [mainGameLayer getChildByTag:245]) != nil) {
                [aChild removeFromParentAndCleanup:YES];
            }
            
            
            if ((sprite.position.x < 2*pixelSize) || (sprite.position.x > (480-2*pixelSize))) {
                
                CCSprite *proximityIndicatorX1 = [CCSprite spriteWithFile:@"whitepixel.png"];
                proximityIndicatorX1.textureRect = CGRectMake(0, 0, 2, pixelSize);
                proximityIndicatorX1.position = ccp(1,sprite.position.y);
                proximityIndicatorX1.color = _enemyColor;
                proximityIndicatorX1.tag = 245;
                
                [mainGameLayer addChild:proximityIndicatorX1 z: 500];
                
                
                CCSprite *proximityIndicatorX2 = [CCSprite spriteWithFile:@"whitepixel.png"];
                proximityIndicatorX2.textureRect = CGRectMake(0, 0, 2, pixelSize);
                proximityIndicatorX2.position = ccp(479,sprite.position.y);
                proximityIndicatorX2.color = _enemyColor;
                proximityIndicatorX2.tag = 245;
                
                [mainGameLayer addChild:proximityIndicatorX2 z: 500];
                
                
                
                
                
                //do gradient glow
                
                CCSprite *proximityGradientX1 = [self clippingGradientSpriteWithColor:_enemyColor textureSize:pixelSize];
                proximityGradientX1.position = ccp(pixelSize/2,sprite.position.y);
                proximityGradientX1.tag = 245;
                
                proximityGradientX1.rotation = 90;
                proximityGradientX1.opacity = 40;
                
                [mainGameLayer addChild:proximityGradientX1 z:498];
                
                
                CCSprite *proximityGradientX2 = [self clippingGradientSpriteWithColor:_enemyColor textureSize:pixelSize];
                proximityGradientX2.position = ccp(480-pixelSize/2,sprite.position.y);
                proximityGradientX2.tag = 245;
                
                proximityGradientX2.rotation = -90;
                proximityGradientX2.opacity = 40;
                
                [mainGameLayer addChild:proximityGradientX2 z:498];
                
                
                if ((sprite.position.x < pixelSize) || (sprite.position.x > (480-pixelSize))) {
                    
                    CCSprite *proximityIndicatorX1 = [CCSprite spriteWithFile:@"whitepixel.png"];
                    proximityIndicatorX1.textureRect = CGRectMake(0, 0, 2, 3*pixelSize);
                    proximityIndicatorX1.position = ccp(1,sprite.position.y);
                    proximityIndicatorX1.color = ccc3((_enemyColor.r+_bgColor1.r)/2,(_enemyColor.g+_bgColor1.g)/2,(_enemyColor.b+_bgColor1.b)/2);
                    proximityIndicatorX1.tag = 245;
                    
                    [mainGameLayer addChild:proximityIndicatorX1 z: 499];
                    
                    
                    
                    CCSprite *proximityIndicatorX2 = [CCSprite spriteWithFile:@"whitepixel.png"];
                    proximityIndicatorX2.textureRect = CGRectMake(0, 0, 1, 3*pixelSize);
                    proximityIndicatorX2.position = ccp(479,sprite.position.y);
                    proximityIndicatorX2.color = ccc3((_enemyColor.r+_bgColor1.r)/2,(_enemyColor.g+_bgColor1.g)/2,(_enemyColor.b+_bgColor1.b)/2);
                    proximityIndicatorX2.tag = 245;
                    
                    [mainGameLayer addChild:proximityIndicatorX2 z: 499];
                    
                    //
                    proximityGradientX1.opacity = 80;
                    proximityGradientX2.opacity = 80;
                    
                }
                
                
                
            }
            
            if ((sprite.position.y < 2*pixelSize) || (sprite.position.y > (320-2*pixelSize))) {
                //NSLog(@"hero near clip y");
                
                CCSprite *proximityIndicatorY1 = [CCSprite spriteWithFile:@"whitepixel.png"];
                proximityIndicatorY1.textureRect = CGRectMake(0, 0, pixelSize, 2);
                proximityIndicatorY1.position = ccp(sprite.position.x,1);
                proximityIndicatorY1.color = _enemyColor;
                proximityIndicatorY1.tag = 245;
                
                [mainGameLayer addChild:proximityIndicatorY1 z: 500];
                
                CCSprite *proximityIndicatorY2 = [CCSprite spriteWithFile:@"whitepixel.png"];
                proximityIndicatorY2.textureRect = CGRectMake(0, 0, pixelSize, 2);
                proximityIndicatorY2.position = ccp(sprite.position.x,319);
                proximityIndicatorY2.color = _enemyColor;
                proximityIndicatorY2.tag = 245;
                
                [mainGameLayer addChild:proximityIndicatorY2 z: 500];
                
                
                
                //do gradient glow
                
                CCSprite *proximityGradientY1 = [self clippingGradientSpriteWithColor:_enemyColor textureSize:pixelSize];
                proximityGradientY1.position = ccp(sprite.position.x,pixelSize/2);
                proximityGradientY1.tag = 245;
                
                proximityGradientY1.rotation = 0;
                proximityGradientY1.opacity = 40;
                
                [mainGameLayer addChild:proximityGradientY1 z:498];
                
                
                CCSprite *proximityGradientY2 = [self clippingGradientSpriteWithColor:_enemyColor textureSize:pixelSize];
                proximityGradientY2.position = ccp(sprite.position.x, 320-pixelSize/2);
                proximityGradientY2.tag = 245;
                
                proximityGradientY2.rotation = 180;
                proximityGradientY2.opacity = 40;
                
                [mainGameLayer addChild:proximityGradientY2 z:498];
                
                
                
                if ((sprite.position.y < pixelSize) || (sprite.position.y > (320-pixelSize))) {
                    
                    CCSprite *proximityIndicatorY1 = [CCSprite spriteWithFile:@"whitepixel.png"];
                    proximityIndicatorY1.textureRect = CGRectMake(0, 0, pixelSize*3, 2);
                    proximityIndicatorY1.position = ccp(sprite.position.x,1);
                    proximityIndicatorY1.color = ccc3((_enemyColor.r+_bgColor1.r)/2,(_enemyColor.g+_bgColor1.g)/2,(_enemyColor.b+_bgColor1.b)/2);
                    proximityIndicatorY1.tag = 245;
                    
                    [mainGameLayer addChild:proximityIndicatorY1 z: 499];
                    
                    CCSprite *proximityIndicatorY2 = [CCSprite spriteWithFile:@"whitepixel.png"];
                    proximityIndicatorY2.textureRect = CGRectMake(0, 0, pixelSize*3, 2);
                    proximityIndicatorY2.position = ccp(sprite.position.x,319);
                    proximityIndicatorY2.color = ccc3((_enemyColor.r+_bgColor1.r)/2,(_enemyColor.g+_bgColor1.g)/2,(_enemyColor.b+_bgColor1.b)/2);
                    proximityIndicatorY2.tag = 245;
                    
                    [mainGameLayer addChild:proximityIndicatorY2 z: 499];
                    
                    //
                    proximityGradientY1.opacity = 80;
                    proximityGradientY2.opacity = 80;
                    
                }
                
            }
            
        }
        
        
    }
}


//
// GENERATE GRADIENT TEXTURE FOR CLIPPING PROXIMITY INDICATOR/INCOMING COLLECTIBLE INDICATOR
// generate texture of tile size
// draw gradient from supplied color with full alpha to zero alpha
//
-(CCSprite *)clippingGradientSpriteWithColor:(ccColor3B)bgColor textureSize:(float)textureSize {
    
    // 1: Create new CCRenderTexture
    CCRenderTexture *rt = [CCRenderTexture renderTextureWithWidth:textureSize height:textureSize];
    
    // 2: Call CCRenderTexture:begin
    [rt beginWithClear:bgColor.r/255.0f g:bgColor.g/255.0f b:bgColor.b/255.0f a:0.0f];
    
    // 3: Draw into the texture
    // You'll add this later
    
    self.shaderProgram = [[CCShaderCache sharedShaderCache] programForKey:kCCShader_PositionColor];
    
    CC_NODE_DRAW_SETUP();
    
    // 3: Draw into the texture
    float gradientAlpha = 0.7f;
    CGPoint vertices[4];
    ccColor4F colors[4];
    int nVertices = 0;
    
    vertices[nVertices] = CGPointMake(0, 0);
    colors[nVertices++] = (ccColor4F){bgColor.r/255.0f, bgColor.g/255.0f, bgColor.b/255.0f, 0 };
    vertices[nVertices] = CGPointMake(textureSize, 0);
    colors[nVertices++] = (ccColor4F){bgColor.r/255.0f, bgColor.g/255.0f, bgColor.b/255.0f, 0};
    vertices[nVertices] = CGPointMake(0, textureSize);
    colors[nVertices++] = (ccColor4F){bgColor.r/255.0f, bgColor.g/255.0f, bgColor.b/255.0f, gradientAlpha};
    vertices[nVertices] = CGPointMake(textureSize, textureSize);
    colors[nVertices++] = (ccColor4F){bgColor.r/255.0f, bgColor.g/255.0f, bgColor.b/255.0f, gradientAlpha};
    
    ccGLEnableVertexAttribs(kCCVertexAttribFlag_Position  | kCCVertexAttribFlag_Color);
    
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_FLOAT, GL_FALSE, 0, colors);
    glBlendFunc(CC_BLEND_SRC, CC_BLEND_DST);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)nVertices);
    
    
    
    // 4: Call CCRenderTexture:end
    [rt end];
    
    // 5: Create a new Sprite from the texture
    return [CCSprite spriteWithTexture:rt.sprite.texture];
    
}


//Time countodown at the start of the game
//TO DO: change later to shorter countdown
-(void)countdownToStart {
    
    
    
    if (countdownNum > 0) {
        
        if (countdownCount > 0) {
            
            countdownCount--;
            
        } else {
            
            countdownCount = 60;
            countdownNum--;
            NSLog(@"TIME TO START %i", countdownNum);
            
            [countdownLabel setVisible:true];
            
            switch (countdownNum) {
                case 4:
                    
                    [countdownLabel setString: @"3"];
                    [countdownLabel setFontSize:66.0f];
                    [self labelScaleAnimation];
                    
                    break;
                    
                case 3:
                    
                    [countdownLabel setString: @"2"];
                    [countdownLabel setFontSize:66.0f];
                    [self labelScaleAnimation];
                    
                    break;
                    
                    
                case 2:
                    
                    [countdownLabel setString: @"1"];
                    [countdownLabel setFontSize:66.0f];
                    [self labelScaleAnimation];
                    
                    break;
                    
                case 1:
                    
                    
                    if (isPause == NO) {
                        
                        
                        //labels for different game modes
                        if (gameMode == GM_COLLECT) {
                            [countdownLabel setString: @"TILT TO COLLECT!"];
                        } else if (gameMode == GM_SURVIVE) {
                            [countdownLabel setString: @"TILT TO SURVIVE!"];
                        } else {
                            [countdownLabel setString: @"TILT TO DO BOTH!"];
                        }
                        
                    }
                    
                    [countdownLabel setFontSize:45.0f];
                    [countdownLabel setPosition:ccp(240,100)];
                    [self labelScaleAnimation];
                    
                    [self gameStartAnimation];
                    
                    if (isPause == YES) {
                        isPause = NO;
                        
                        [self resumeAllActionsAfterPause];
                    }
                    
                    break;
                    
                default:
                    break;
            }
            
        }
        
        
    } else {
        
        isStart = YES;
        
    }
    
}

-(void)labelScaleAnimation {
    
    [self gameCountdownBlinkAnimation];
    
    countdownLabel.scale = 1.0f;
    countdownLabel.opacity  = 255;
    
    [countdownLabel runAction:[CCSpawn actions:
                               [CCScaleBy actionWithDuration:0.8f scale:0.5f],
                               [CCSequence actions:
                                [CCDelayTime actionWithDuration:0.2f],
                                [CCFadeOut actionWithDuration:0.5f],
                                nil],
                               nil]
     ];
    
}

-(void)gameCountdownBlinkAnimation {
    
    id fadeOut = [CCFadeTo actionWithDuration:0.45f opacity:175];
    id fadeIn = [CCFadeTo actionWithDuration:0.45f opacity:255];
    
    id fadeSequence = [CCSequence actions:fadeOut,fadeIn,nil];
    
    [movingSquare runAction:[CCSpawn actions:fadeSequence, nil]];
}

-(void)gameStartAnimation {
    
    CCSprite* gameStartAnimationSprite = [CCSprite spriteWithFile:@"whitepixel.png"];
    gameStartAnimationSprite.textureRect = CGRectMake(0, 0, pixelSize, pixelSize);
    gameStartAnimationSprite.position = movingSquare.position;
    
    gameStartAnimationSprite.color = _heroColor;
    gameStartAnimationSprite.opacity = 220;
    
    [mainGameLayer addChild:gameStartAnimationSprite z:(movingSquare.zOrder-1)];
    
    [gameStartAnimationSprite runAction:[CCSpawn actions:
                                         [CCSequence actions:[CCScaleBy actionWithDuration:0.4f scale:2.0f],
                                          [CCCallFuncN actionWithTarget:self selector:@selector(autoDestroyCollectible:)],
                                          nil],
                                         [CCSequence actions:
                                          [CCDelayTime actionWithDuration:0.05f],
                                          [CCFadeOut actionWithDuration:0.3f],
                                          nil],
                                         nil]
     ];
    
    
}



-(void)followSpriteSpawnAnimation:(subSprite*)sprite {
    
    
    CCSprite* followSpriteSpawnAnimationSprite = [CCSprite spriteWithFile:@"whitepixel.png"];
    followSpriteSpawnAnimationSprite.textureRect = CGRectMake(0, 0, pixelSize, pixelSize);
    followSpriteSpawnAnimationSprite.position = sprite.position;
    
    followSpriteSpawnAnimationSprite.color = _enemyColor;
    followSpriteSpawnAnimationSprite.opacity = 220;
    
    [mainGameLayer addChild:followSpriteSpawnAnimationSprite z:(sprite.zOrder-1)];
    
    [followSpriteSpawnAnimationSprite runAction:[CCSpawn actions:
                                                 [CCSequence actions:[CCScaleBy actionWithDuration:0.4f scale:2.0f],
                                                  [CCCallFuncN actionWithTarget:self selector:@selector(autoDestroyCollectible:)],
                                                  nil],
                                                 [CCSequence actions:
                                                  [CCDelayTime actionWithDuration:0.05f],
                                                  [CCFadeOut actionWithDuration:0.3f],
                                                  nil],
                                                 nil]
     ];
    
}


//
// START TIMER
// if game mode is COLLECT, timer counts DOWN from a predetermined game duration
// if timer runs out, execute GAME OVER
// for any other game mode (SURVIVE, CLASSIC), timer counts UP
-(void)startTimer {
    
    if (gameMode == GM_COLLECT) {
        
        int timeFramesLeft = gameTimeFrames - timerCount;
        
        if (timeFramesLeft > 0) {
            
            int framesToSeconds = (int)ceil(timeFramesLeft/60);
            
            int minutes = (int)floor(framesToSeconds/60);
            
            int seconds = framesToSeconds - minutes*60;
            
            
            [timerLabel setString:[NSString stringWithFormat:@"%d:%02d", minutes, seconds]];
            
        } else {
            
            [self gameOverMan];
            
        }
        
    } else {
        
        int framesToSeconds = (int)floor(timerCount/60);
        
        int minutes = (int)floor(framesToSeconds/60);
        
        int seconds = framesToSeconds - minutes*60;
        
        
        [timerLabel setString:[NSString stringWithFormat:@"%d:%02d", minutes, seconds]];
        
    }
    
}

//
// MAIN GAME OVER FUNCTION
// call game over screen after delay
//
-(void) gameOverMan {
    
    gameOver = YES;
    NSLog(@"game over :(");
    
    
    [self gameOverDetail];
    
    
    // delay for game over layer
    // TO DO: replace with action
    double delayInSeconds = 2.0f;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self doGameOver];
    });
    
    
}

//
// HANDLE GAME OVER
// loop through objects and stop animations/actions
// change colors of objects/replace textures
// TO DO: remove objects, add graphics
//
-(void) gameOverDetail {
    
    
    /// PAUSE STUFF
    
    CCArray *loopArray = [[mainGameLayer children] copy];
    
    
    for (id obj in loopArray) {
        
        if ([obj isMemberOfClass:[subSprite class]]) {
            
            
            subSprite *sprite = (subSprite*)obj;
            
            if (sprite != NULL && sprite.visible != NO) {
                
                if (sprite.tag == 60) {
                    
                    [[[CCDirector sharedDirector] actionManager] pauseTarget:sprite];
                    sprite.color = _hudColor1;
                }
            }
        }
    }
    
    
    [movingSquare setTexture:[self genTextureWithColor:_hudColor1 textureSize:movingSquare.contentSize.width sprite:movingSquare direction:0].sprite.texture];
    
    
}

//
// GAME OVER OVERLAY
//
-(void) doGameOver {
    
    
    //
    //game is over - enable auto-lock timer
    //
    
    [self changeAutoLock];
    
    //
    //////
    //
    
    
    
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    gameOverLayer = [CCLayerColor layerWithColor:ccc4(_bgColor1.r, _bgColor1.g, _bgColor1.b, 250)];
    [gameOverLayer setContentSize:size];
    gameOverLayer.anchorPoint = ccp(0,0);
    gameOverLayer.position = ccp(0,0);
    
    [self addChild:gameOverLayer z:15000];
    
    
    CCLabelTTF *gameOverLabel = [CCLabelTTF labelWithString:@"GAME OVER" fontName:@"04b03" fontSize:22.0f];
    gameOverLabel.position = ccp(size.width/2,size.height/2+60);
    
    gameOverLabel.color = _hudColor1;
    
    [gameOverLayer addChild:gameOverLabel z:1];
    
    int sendScoreInt = 0;
    
    NSString *scoreString = @"";
    NSString *fakeScore = @"";
    
    
    //TEST MODE STUFF - FAKE SCORE
    //TO DO: used for testing replace with actual score chart fetched from the server
    //
    if (gameMode == GM_COLLECT) {
        sendScoreInt = scoreCount;
        
        scoreString = [NSString stringWithFormat:@"%d", scoreCount];
        fakeScore = @"Ziga's top score is 87";
        
    } else if (gameMode == GM_SURVIVE){
        
        float timeInSeconds = timerCount/60.0f;
        sendScoreInt = timerCount;
        scoreString = [NSString stringWithFormat:@"%.02f",timeInSeconds];
        
        fakeScore = @"Ziga survived for 65.78 seconds";
        
    } else {
        
        
        float timeInSeconds = timerCount/60.0f;
        sendScoreInt = timerCount+scoreCount;
        scoreString = [NSString stringWithFormat:@"%.02f + %d",timeInSeconds,scoreCount];
        
        fakeScore = @"Ziga survived for 34.82 seconds and scored 43";
        
    }
    
    
    
    /////////
    
    if (_testingMode == true) {
        
        
        CCLabelTTF *fakeScoreLabel = [CCLabelTTF labelWithString:fakeScore fontName:@"04b03" fontSize:22.0];
        
        fakeScoreLabel.position = ccp(size.width/2, size.height/2-50);
        fakeScoreLabel.color = _hudColor1;
        
        [gameOverLayer addChild:fakeScoreLabel];
        
    }
    
    /////////
    
    
    CCLabelTTF *gameScoreLabel = [CCLabelTTF labelWithString:scoreString fontName:@"04b03" fontSize:44.0f];
    
    gameScoreLabel.position = ccp(size.width/2,size.height/2);
    gameScoreLabel.color = _hudColor1;
    
    [gameOverLayer addChild:gameScoreLabel];
    
    
    // Default font size will be 22 points.
	[CCMenuItemFont setFontSize:22];
    [CCMenuItemFont setFontName:@"04b03"];
    
    
    
    CCMenuItemLabel *reset = [CCMenuItemFont itemWithString:@"restart" target:self selector:@selector(restart)];
    
    
    CCMenuItemLabel *menuBtn = [CCMenuItemFont itemWithString:@"back" target:self selector:@selector(backToMenu)];
    
    
    reset.anchorPoint = ccp(0.5,0.5);
    reset.position = ccp(size.width/2+50, size.height/2-100);
    reset.color = _hudColor1;
	
    menuBtn.anchorPoint = ccp(0.5,0.5);
    menuBtn.position = ccp(size.width/2-50, size.height/2-100);
    menuBtn.color = _hudColor1;
    
	
    CCMenu *menu = [CCMenu menuWithItems:/*itemAchievement, itemLeaderboard, */menuBtn, reset, nil];
    
	
    //[menu setPosition:ccp( size.width/2, size.height/2)];
    [menu setPosition:ccp(0,0)];
	
    
    //analytics at gameover
    [[[GAI sharedInstance] defaultTracker] sendEventWithCategory:@"game_over_event" withAction:[NSString stringWithFormat:@"gameOver_mode_%d",gameModeInt] withLabel:@"gameCompleted" withValue:[NSNumber numberWithInt:sendScoreInt]];
    
    if (_testingMode != true) {
        
        //do stuff on game over
        
    }
    
    [gameOverLayer addChild: menu z:1];
    
}

//PAUSE LAYER
-(void) doPause {
    
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    pauseLayer = [CCLayerColor layerWithColor:ccc4(_bgColor1.r, _bgColor1.g, _bgColor1.b, 250)];
    [pauseLayer setContentSize:size];
    pauseLayer.anchorPoint = ccp(0,0);
    pauseLayer.position = ccp(0,0);
    
    [self addChild:pauseLayer z:15000];
    
    
    CCLabelTTF *pauseLabel = [CCLabelTTF labelWithString:@"GAME PAUSED" fontName:@"04b03" fontSize:22.0f];
    pauseLabel.position = ccp(size.width/2,size.height/2+60);
    
    pauseLabel.color = _hudColor1;
    
    [pauseLayer addChild:pauseLabel z:1];
    
    // Default font size will be 22 points.
    [CCMenuItemFont setFontSize:22];
    [CCMenuItemFont setFontName:@"04b03"];
    
    
    CCMenuItemLabel *resume = [CCMenuItemFont itemWithString:@"play" target:self selector:@selector(resumeGame)];
    
    
    CCMenuItemLabel *reset = [CCMenuItemFont itemWithString:@"restart" target:self selector:@selector(restart)];
    
    
    CCMenuItemLabel *menuBtn = [CCMenuItemFont itemWithString:@"back" target:self selector:@selector(backToMenu)];
    
    
    resume.anchorPoint = ccp(0.5,0.5);
    resume.position = ccp(size.width/2, size.height/2);
    resume.color = _hudColor1;
    
    
    reset.anchorPoint = ccp(0.5,0.5);
    reset.position = ccp(size.width/2+50, size.height/2-60);
    reset.color = _hudColor1;
	
    menuBtn.anchorPoint = ccp(0.5,0.5);
    menuBtn.position = ccp(size.width/2-50, size.height/2-60);
    menuBtn.color = _hudColor1;
    
	
    CCMenu *menu = [CCMenu menuWithItems:/*itemAchievement, itemLeaderboard, */resume, menuBtn, reset, nil];
    
	
    //[menu setPosition:ccp( size.width/2, size.height/2)];
    [menu setPosition:ccp(0,0)];
	
    [pauseLayer addChild: menu z:1];
    
}




//
// TIMER TO SPAWN COLLECTIBLES
// if timer matches collectible time, create new collectible, reset timer and choose a new random spawn time
// else, increase timer count
//
- (void)timeCollectibles {
    
    if (collectibleTimerCount == spawnCollectibleTime) {
        
        collectibleTimerCount = 0;
        
        spawnCollectibleTime = [self doRandomFrom:minTime To:maxTime];
        
        [self spawnCollectible];
        
    } else {
        collectibleTimerCount++;
    }
    
}

//
// CHECK FOR COLLISIONS BETWEEN HERO AND OBJECTS
// loop through array of mainGameLayer children and check for proximity between HERO and other game objects
//
- (void)checkCollisions {
    
    CCArray *loopArray = [[mainGameLayer children] copy];
    
    //for (CCSprite *sprite in self.children) {
    
    for (id obj in loopArray) {
        
        if ([obj isMemberOfClass:[subSprite class]]) {
            
            
            subSprite *sprite = (subSprite*)obj;
            
            if (sprite != NULL && sprite.visible != NO) {
                
                if (sprite.tag == 35 || sprite.tag == 36) { // enemies and you
                    
                    
                    //check Explosion Collision
                    [self explodeCollisionsForSprite:sprite];
                    
                    //collisions between enemy and hero
                    float distanceApart = ccpDistance(movingSquare.position, sprite.position);
                    
                    if (distanceApart < pixelSize) {
                        
                        NSLog(@"boom, collision at timerCount %d", timerCount);
                        
                        
                        
                        sprite.tag = 40;
                        sprite.opacity = 255;
                        
                        
                        [sprite setTexture:[self genTextureWithColor:_hudColor1 textureSize:sprite.contentSize.width sprite:sprite direction:0].sprite.texture];
                        
                        [self reorderChild:sprite z:1200];
                        
                        [self gameOverMan];
                        
                        
                        
                    }
                    
                } else
                    
                    
                    if (sprite.tag == 60) { // collectible and you
                        
                        float distanceApart = ccpDistance(movingSquare.position, sprite.position);
                        
                        if (distanceApart < pixelSize) {
                            
                            [self grabCollectible:sprite];
                            
                            
                        }
                        
                    }
            }
        }
    }
    
    
}


#pragma mark COMBOS

NSMutableArray *collectionsTimeArray;


int collectComboCount = 0;
int lastCollectType = 0;

bool isSequenceCombo = false;

#define collectTypeComboAmount 3 //number of same color collects needed for COMBO
#define collectSequenceComboAmount 3 //number of collects in limited time needed for COMBO
#define collectSequenceComboTime 2.0f //combo sequence time limit COMBO


//
// CHECK FOR COMBO SEQUENCE
// create array to keep track of collectibles
// for every collect, write current time in array
// check array if last 3 objects are within a combo time frame - do combo function
//
- (void)checkForSequenceCombo {
    
    if (!collectionsTimeArray) {
        
        collectionsTimeArray = [NSMutableArray new];
        
        [collectionsTimeArray addObject:[NSNumber numberWithInt:timerCount]];
        
    } else {
        
        [collectionsTimeArray addObject:[NSNumber numberWithInt:timerCount]];
        
        if (collectionsTimeArray.count >= collectSequenceComboAmount) {
            
            int timeInFrames = round(collectSequenceComboTime*60);
            
            int lastCollectTime = [[collectionsTimeArray objectAtIndex:(collectionsTimeArray.count-1)] intValue];
            
            int firstInSequenceCollectTime = [[collectionsTimeArray objectAtIndex:(collectionsTimeArray.count-collectSequenceComboAmount)] intValue];
            
            if ((lastCollectTime-firstInSequenceCollectTime) < timeInFrames) {
                
                [self doComboType:2];
                
                isSequenceCombo = true;
                
            } else {
                
                if (isSequenceCombo == true) {
                    
                    [self doComboBreaker];
                    isSequenceCombo = false;
                    
                }
            }
            
        }
        
    }
    
}

//
// CHECK FOR SAME TYPE COMBO
// check if previous collectible was of same type
// if yes, increase combo count, else reset
// if combo count is 3 or more, do combo action
//
- (void)checkForCollectCombo:(subSprite*)sprite {
    
    if (sprite.collectibleType == lastCollectType) { // collected the same as before
        
        collectComboCount++;
        
        
    } else { //collectible is different
        
        
        if (collectComboCount>collectTypeComboAmount) { // was combo mode/combo breaker
            [self doComboBreaker];
        }
        
        collectComboCount = 1;
        lastCollectType = sprite.collectibleType;
        
    }
    
    
    if ((collectComboCount % collectTypeComboAmount) == 0) { //combo multiplier
        
        [self doComboType:1];
    }
}

- (void)doComboBreaker {
    
    NSLog(@"C-C-C-COMBO BREAKER");
    
    [[[GAI sharedInstance] defaultTracker] sendEventWithCategory:@"game_event" withAction:@"combo_Breaker" withLabel:@"inGameEvent" withValue:[NSNumber numberWithInt:-1]];
    
    scoreMult = 1;
}


- (void)doComboType:(int)comboType {
    
    
    //HANDLE COMBOS
    
    switch (comboType) {
        case 1:
            NSLog(@"yay, combo type %d - same color, with multiplier %d",comboType,(collectComboCount/collectTypeComboAmount));
            break;
            
        case 2:
            NSLog(@"yay, combo type %d - comboSequence",comboType);
            break;
            
        default:
            break;
    }
    
    scoreMult = 1*floor(collectComboCount/collectTypeComboAmount); //set score multiplier from combo
    
    [self doScore:10]; //add 10 score
    
    
    //google analytics event - COMBO
    [[[GAI sharedInstance] defaultTracker] sendEventWithCategory:@"game_event" withAction:[NSString stringWithFormat:@"combo_%d_%dx",comboType,(collectComboCount/collectTypeComboAmount)] withLabel:@"inGameEvent" withValue:[NSNumber numberWithInt:-1]];
    
    
    
    CCLabelTTF *comboLabel = [CCLabelTTF labelWithString:@"COMBO" fontName:@"04b03" fontSize:32.0f];
    
    comboLabel.position = ccp(240,160);
    comboLabel.color = _hudColor1;
    
    [self addChild:comboLabel z:1500];
    
    
    
    id scaleAnim = [CCScaleBy actionWithDuration:0.4f scale:1.75f];
    
    id fadeAnim = [CCFadeTo actionWithDuration:0.3f opacity:0];
    
    id animationSpawn = [CCSpawn actions:scaleAnim, fadeAnim, nil];
    
    id removeAnimSprite = [CCCallFuncN actionWithTarget:self selector:@selector(autoDestroyCollectible:)];
    
    
    id removeSequence = [CCSequence actions: animationSpawn, removeAnimSprite, nil];
    
    
    [comboLabel runAction:removeSequence];
    
    
}

int scoreMult = 1; //score multiplier

-(void) doScore:(int)add {
    
    scoreCount = scoreCount+(add*scoreMult); //increase score count
    
    NSLog(@"current score count %d", scoreCount);
    
    [scoreLabel setString: [NSString stringWithFormat:@"%d",scoreCount]];
    
}


//
// COLLECTIBLE WAS PICKED UP
// check for combos
// add to score
// create animated trail on collect
// destroy collected sprite
//
- (void)grabCollectible:(subSprite*)sprite {
    
    
    [self checkForCollectCombo:sprite];
    
    [self checkForSequenceCombo];
    
    
    [self doScore:1];
    
    
    
    [self grabCollectibleSpriteCreate:sprite];
    
    
    [self destroyCollectible:sprite];
    
    
}

//
// create trail on collect
//
- (void) grabCollectibleSpriteCreate:(subSprite*)sprite {
    
    ccColor3B useThisColor = sprite.savedColor;
    
    
    if (_testingMode == false) {
        
        subSprite *trailSpriteBlocks;
        
        
        trailSpriteBlocks = [subSprite spriteWithTexture:[self genTrailTextureWithColor:useThisColor textureSize:pixelSize/2 sprite:trailSpriteBlocks tiles:2].sprite.texture];
        
        trailSpriteBlocks.savedColor = useThisColor;
        trailSpriteBlocks.position = sprite.position;
        trailSpriteBlocks.tag = 74;
        
        [mainGameLayer addChild:trailSpriteBlocks z:992];
        
        
    }
    
    CCSprite* collectibleGrabSprite = [CCSprite spriteWithFile:@"whitepixel.png"];
    collectibleGrabSprite.textureRect = CGRectMake(0, 0, pixelSize/2, pixelSize/2);
    collectibleGrabSprite.position = sprite.position;//p;
    
    //make collectibleAnimationSprite the same color as sprite was
    collectibleGrabSprite.color = sprite.savedColor;
    
    
    collectibleGrabSprite.opacity = 220;
    collectibleGrabSprite.tag = 70;
    
    [mainGameLayer addChild:collectibleGrabSprite z:990];
    
    [self grabCollectibleAnimationForSprite:collectibleGrabSprite];
    
    
    
    
}

- (void) grabCollectibleAnimationForSprite:(CCSprite*)sprite{
    
    id scaleAnim = [CCScaleBy actionWithDuration:0.4f scale:1.75f];
    
    id fadeAnim = [CCFadeTo actionWithDuration:0.3f opacity:0];
    
    id animationSpawn = [CCSpawn actions:scaleAnim, fadeAnim, nil];
    
    id removeAnimSprite = [CCCallFuncN actionWithTarget:self selector:@selector(autoDestroyCollectible:)];
    
    
    id removeSequence = [CCSequence actions: animationSpawn, removeAnimSprite, nil];
    
    
    [sprite runAction:removeSequence];
    
    
}

//
// MOVEMENT OF (smart) FOLLOW ENEMY
// calculate distance by X and by Y between ENEMY and HERO
// if clipping for ENEMY disabled, check if distance between HERO and ENEMY is shorter on X or Y than half the game area width/height
// calculate distance "through" wall
// choose shorter distance
// randomly choose direction (X or Y axis movement)
// move by one tile
//
- (void)moveFollowEnemy:(subSprite*)sprite {
    
    CGSize size = CGSizeMake(steviloPoX*pixelSize,steviloPoY*pixelSize);
    
    CGPoint ep = sprite.position;
    CGPoint mp = movingSquare.position;
    
    
    int x1 = mp.x - ep.x;
    int y1 = mp.y - ep.y;
    
    int x = x1;
    int y = y1;
    
    if (_enemyClip == false) {
        
        int x2 = NULL;
        int y2 = NULL;
        
        if (ep.x > size.width/2) {
            
            x2 = (size.width+mp.x) - ep.x;
            
        } else {
            
            x2 = -(size.width-mp.x) - ep.x;
            
        }
        
        if (ep.y > size.height/2) {
            
            y2 = (size.height+mp.y) - ep.y;
            
        } else {
            
            y2 = -(size.height-mp.y) - ep.y;
            
        }
        
        
        if (abs(x2) > abs(x1)) {
            x = x1;
        } else {
            x = x2;
        }
        
        if (abs(y2) > abs(y1)) {
            y = y1;
        } else {
            y = y2;
        }
        
        
        
    }
    
    
    
    
    int moveDirection = 0; //1=up,2=down,3=right,4=left
    
    
    if (x == 0) {
        
        if (y > 0)
            moveDirection = 1; //up
        else
            moveDirection = 2; //down
        
    } else if (y == 0) {
        
        if (x > 0)
            moveDirection = 3; //right
        else
            moveDirection = 4; //left
        
    } else {
        
        switch ([self doRandomFrom:1 To:2]) {
            case 1: //vertical move
                
                if (y > 0)
                    moveDirection = 1; //up
                else
                    moveDirection = 2; //down
                
                break;
                
            case 2: //horizontal move
                
                if (x > 0)
                    moveDirection = 3; //right
                else
                    moveDirection = 4; //left
                
                break;
                
                
            default:
                break;
        }
        
        
        
    }
    
    
    switch (moveDirection) {
        case 1:
            
            if (ep.y < size.height-pixelSize/2) {
                
                [self leaveEnemyTrail:ep];
                
                [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:1].sprite.texture];
                [sprite setPosition:ccp(ep.x, ep.y+pixelSize)];
                
            } else {
                
                if (_enemyClip == false) {
                    
                    [self leaveEnemyTrail:ep];
                    
                    [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:1].sprite.texture];                    [sprite setPosition:ccp(ep.x,pixelSize/2)];
                    
                }
                
            }
            
            break;
            
        case 2:
            
            if (ep.y > pixelSize/2) {
                
                [self leaveEnemyTrail:ep];
                
                [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:2].sprite.texture];                [sprite setPosition:ccp(ep.x, ep.y-pixelSize)];
                
            } else {
                
                if (_enemyClip == false) {
                    
                    [self leaveEnemyTrail:ep];
                    
                    [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:2].sprite.texture];                    [sprite setPosition:ccp(ep.x,size.height-pixelSize/2)];
                    
                }
                
            }
            
            break;
            
        case 3:
            
            if (ep.x < size.width-pixelSize/2) {
                
                [self leaveEnemyTrail:ep];
                
                [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:3].sprite.texture];                [sprite setPosition:ccp(ep.x+pixelSize, ep.y)];
                
            } else {
                
                if (_enemyClip == false) {
                    
                    [self leaveEnemyTrail:ep];
                    
                    [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:3].sprite.texture];                    [sprite setPosition:ccp(pixelSize/2, ep.y)];
                    
                }
                
            }
            
            break;
            
        case 4:
            
            if (ep.x > pixelSize/2) {
                
                [self leaveEnemyTrail:ep];
                
                [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:4].sprite.texture];                [sprite setPosition:ccp(ep.x-pixelSize, ep.y)];
                
            } else {
                
                if (_enemyClip == false) {
                    
                    [self leaveEnemyTrail:ep];
                    
                    [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:4].sprite.texture];                    [sprite setPosition:ccp(size.width-pixelSize/2,ep.y)];
                    
                }
                
            }
            
            break;
            
        default:
            
            break;
    }
    
    if (moveDirection != 0) {
        
        [self checkClippingEnemy:sprite];
    }
}

//
// MOVEMENT OF (dumb) ENEMY
// choose random direction (or stand still)
// do movement
// if enemy clipping disabled, move "through" wall (tile closest to the opposite edge for what ever direction you choose)
//
- (void)moveEnemy:(subSprite*)sprite {
    
    CGSize size = CGSizeMake(steviloPoX*pixelSize,steviloPoY*pixelSize);//[CCDirector sharedDirector].winSize;
    
    CGPoint enemyPos = sprite.position;
    
    int directionMove = [self doRandomFrom:0 To:4];
    
    switch (directionMove) {
        case 1:
            
            if (enemyPos.y < size.height-pixelSize/2) {
                
                [self leaveEnemyTrail:enemyPos];
                
                [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:1].sprite.texture];                [sprite setPosition:ccp(enemyPos.x, enemyPos.y+pixelSize)];
                
            } else {
                
                if (_enemyClip == false) {
                    
                    [self leaveEnemyTrail:enemyPos];
                    
                    [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:1].sprite.texture];                    [sprite setPosition:ccp(enemyPos.x,pixelSize/2)];
                    
                }
                
            }
            break;
            
        case 2:
            
            if (enemyPos.y > pixelSize/2) {
                
                [self leaveEnemyTrail:enemyPos];
                
                [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:2].sprite.texture];                [sprite setPosition:ccp(enemyPos.x, enemyPos.y-pixelSize)];
                
            } else {
                
                if (_enemyClip == false) {
                    
                    [self leaveEnemyTrail:enemyPos];
                    
                    [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:2].sprite.texture];                    [sprite setPosition:ccp(enemyPos.x,size.height-pixelSize/2)];
                    
                }
                
            }
            break;
            
        case 3:
            
            if (enemyPos.x < size.width-pixelSize/2) {
                
                [self leaveEnemyTrail:enemyPos];
                
                [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:3].sprite.texture];                [sprite setPosition:ccp(enemyPos.x+pixelSize, enemyPos.y)];
                
            } else {
                
                if (_enemyClip == false) {
                    
                    [self leaveEnemyTrail:enemyPos];
                    
                    [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:3].sprite.texture];                    [sprite setPosition:ccp(pixelSize/2,enemyPos.y)];
                    
                }
                
            }
            
            break;
            
        case 4:
            
            if (enemyPos.x > pixelSize/2) {
                
                [self leaveEnemyTrail:enemyPos];
                
                [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:4].sprite.texture];                [sprite setPosition:ccp(enemyPos.x-pixelSize, enemyPos.y)];
                
            } else {
                
                if (_enemyClip == false) {
                    
                    [self leaveEnemyTrail:enemyPos];
                    
                    [sprite setTexture:[self genTextureWithColor:_enemyColor textureSize:sprite.contentSize.width sprite:sprite direction:4].sprite.texture];                    [sprite setPosition:ccp(size.width-pixelSize/2,enemyPos.y)];
                    
                }
                
            }
            
            break;
            
        default:
            
            
            break;
    }
    
    if (directionMove != 0) {
        [self checkClippingEnemy:sprite];
    }
    
}

//
// GENERATE TRAIL SPRITE TEXTURE
// create texture with size of tile
// prepare grid of 4 by 4 tiles
// create array for storing colors, and indexes of cells used
// store color for every used cell
// depending on tile count, remove cells from arrays
// draw squares on appropriate grid tiles, based on array
//
-(CCRenderTexture* ) genTrailTextureWithColor:(ccColor3B)useColor textureSize:(int)textureSizeBorder sprite:(subSprite *)sprite tiles:(int)tileCount{
    
    // 1: Create new CCRenderTexture
    CCRenderTexture *rtx = [CCRenderTexture renderTextureWithWidth:textureSizeBorder height:textureSizeBorder];
    
    
    
    // 2: Call CCRenderTexture:begin
    [rtx beginWithClear:useColor.r/255.0f g:useColor.g/255.0f b:useColor.b/255.0f a:0.0f];
    
    // 3: Draw into the texture
    
    
    
    if (_testingMode == false) {
        
        
        
        int textureSize = textureSizeBorder;//-2;
        
        int zamik = (textureSizeBorder-textureSize)/2;
        
        
        int steviloPoXtex = 4/(pixelSize/textureSize);
        int steviloPoYtex = 4/(pixelSize/textureSize);
        
        int elementSize = textureSize/steviloPoXtex;
        
        
        NSMutableArray *cellOptionsArray;
        NSMutableArray *colorArray;
        NSMutableArray *cellsUsedArray;
        
        int trailFrame = 0;
        
        int indexNum = 0;
        
        if (sprite.colorArray != NULL) {
            
            colorArray = sprite.colorArray;
            cellsUsedArray = sprite.cellsUsedArray;
            trailFrame = sprite.trailFrame;
            
            if (cellsUsedArray.count > tileCount) {
                int randomNum = [self doRandomFrom:0 To:(cellsUsedArray.count-1)];
                
                [cellsUsedArray removeObjectAtIndex:randomNum];
            }
            
            
        } else {
            
            cellOptionsArray = [NSMutableArray new];
            
            for (int i = 0; i<steviloPoXtex; i++) {
                //NSLog(@"naredimo array celic po x %d",i);
                [cellOptionsArray addObject:[NSNumber numberWithInt:i]];
            }
            
            
            //NSLog(@"sprite ima user data");
            colorArray = [NSMutableArray new];
            cellsUsedArray = [NSMutableArray new];
            trailFrame = 0;
            
        }
        
        
        
        //rows on Y axis
        for (int y=0; y<steviloPoYtex; y++) {
            
            
            int randomCell = [self doRandomFrom:0 To:3];//[[cellOptionsArray objectAtIndex:randomCellArrayIndex] intValue];
            
            
            //[cellOptionsArray removeObjectAtIndex:randomCellArrayIndex];
            
            //cell in column X and row Y
            for (int x=0; x<steviloPoXtex; x++) {
                
                
                
                indexNum = (y*steviloPoXtex)+x;
                
                ccColor3B randomColorUse;
                
                if (colorArray.count < steviloPoXtex*steviloPoYtex) {
                    
                    
                    //if X equals randomCell, add cell to used cells array
                    if (x == randomCell) {
                        [cellsUsedArray addObject:[NSNumber numberWithInt:indexNum]];
                        
                    }
                    
                    
                    int odmikRandomConst = [self doRandomFrom:-odmikBarve To:odmikBarve];
                    
                    
                    //CLAMPing is so much cooler than if statements
                    
                    int rdeca = clampf((useColor.r+odmikRandomConst), 0, 255);
                    int zelena = clampf((useColor.g+odmikRandomConst), 0, 255);;
                    int modra = clampf((useColor.b+odmikRandomConst), 0, 255);;
                    
                    
                    
                    
                    ccColor3B randomColor = ccc3(rdeca,zelena,modra);
                    
                    
                    
                    
                    
                    [colorArray addObject:[NSValue valueWithBytes:&randomColor objCType:@encode(ccColor3B)]];
                    
                    randomColorUse = randomColor;
                    
                } else {
                    
                    ccColor3B currentColor;
                    
                    [[colorArray objectAtIndex:indexNum] getValue:&currentColor];
                    
                    
                    randomColorUse = currentColor;
                    
                }
                
                //DRAW GRADIENT ON SQUARES
                int gradientShade = (ceil(steviloPoY/2)-y)*gradMult*2;
                
                int rdecaGrad = clampf((randomColorUse.r+gradientShade), 0, 255);
                int zelenaGrad = clampf((randomColorUse.g+gradientShade), 0, 255);;
                int modraGrad = clampf((randomColorUse.b+gradientShade), 0, 255);;
                
                randomColorUse = ccc3(rdecaGrad, zelenaGrad, modraGrad);
                
                int scaleFactorForRetina = CC_CONTENT_SCALE_FACTOR();
                
                //draw squares
                
                if ([cellsUsedArray containsObject:[NSNumber numberWithInt:indexNum]]) {
                    
                    CCSprite *kvadratek = [CCSprite spriteWithFile:@"whitepixel.png"];
                    kvadratek.textureRect = CGRectMake(0,0,elementSize,elementSize);
                    kvadratek.position = ccp((x*elementSize+elementSize/2)+zamik,(y*elementSize+elementSize/2)+zamik);
                    kvadratek.color = randomColorUse;
                    kvadratek.opacity = 255;
                    
                    
                    ccColor3B brightEdgeColor = ccc3(MIN(randomColorUse.r+20,255),MIN(randomColorUse.g+20,255),MIN(randomColorUse.b+20,255));
                    ccColor3B darkEdgeColor = ccc3(MAX(randomColorUse.r-20,0),MAX(randomColorUse.g-20,0),MAX(randomColorUse.b-20,0));
                    
                    //left edge
                    CCSprite *kvadratekBrightRobLevo = [CCSprite spriteWithFile:@"whitepixel.png"];
                    kvadratekBrightRobLevo.textureRect = CGRectMake(0,0,(1.0/scaleFactorForRetina),elementSize);
                    kvadratekBrightRobLevo.position = ccp((0.5/scaleFactorForRetina),elementSize/2);
                    kvadratekBrightRobLevo.color = brightEdgeColor;
                    
                    
                    [kvadratek addChild:kvadratekBrightRobLevo];
                    
                    //upper edge
                    CCSprite *kvadratekBrightRobZgoraj = [CCSprite spriteWithFile:@"whitepixel.png"];
                    kvadratekBrightRobZgoraj.textureRect = CGRectMake(0,0,elementSize,(1.0/scaleFactorForRetina));
                    kvadratekBrightRobZgoraj.position = ccp(elementSize/2,(0.5/scaleFactorForRetina));
                    kvadratekBrightRobZgoraj.color = brightEdgeColor;
                    
                    [kvadratek addChild:kvadratekBrightRobZgoraj];
                    
                    
                    //right edge
                    CCSprite *kvadratekDarkRobDesno = [CCSprite spriteWithFile:@"whitepixel.png"];
                    kvadratekDarkRobDesno.textureRect = CGRectMake(0,0,(1.0/scaleFactorForRetina),elementSize);
                    kvadratekDarkRobDesno.position = ccp((elementSize-(0.5/scaleFactorForRetina)),elementSize/2);
                    kvadratekDarkRobDesno.color = darkEdgeColor;
                    
                    
                    [kvadratek addChild:kvadratekDarkRobDesno];
                    
                    //bottom edge
                    CCSprite *kvadratekDarkRobSpodaj = [CCSprite spriteWithFile:@"whitepixel.png"];
                    kvadratekDarkRobSpodaj.textureRect = CGRectMake(0,0,elementSize,(1.0/scaleFactorForRetina));
                    kvadratekDarkRobSpodaj.position = ccp(elementSize/2,(elementSize-(0.5/scaleFactorForRetina)));
                    kvadratekDarkRobSpodaj.color = darkEdgeColor;
                    
                    [kvadratek addChild:kvadratekDarkRobSpodaj];
                    
                    [kvadratek visit];
                    
                }
                //
                
            }
            
        }
        
        //store data in sprite
        
        sprite.colorArray = colorArray;
        sprite.cellsUsedArray = cellsUsedArray;
        sprite.trailFrame = trailFrame;
        sprite.savedColor = useColor;
        
    }
    
    
    // 4: Call CCRenderTexture:end
    [rtx end];
    
    return rtx;
    
}


-(void) leaveCollectibleTrail:(subSprite *)sprite {
    
    
    
    CCSprite* collectibleTrailSprite = [CCSprite spriteWithFile:@"whitepixel.png"];
    collectibleTrailSprite.textureRect = CGRectMake(0, 0, pixelSize, pixelSize);
    collectibleTrailSprite.position = sprite.position;//p;
    collectibleTrailSprite.color = sprite.savedColor;
    collectibleTrailSprite.opacity = 100;
    collectibleTrailSprite.tag = 70;
    
    [mainGameLayer addChild:collectibleTrailSprite z:990];
    
    [self trailAnimationForSprite:collectibleTrailSprite];
    
}

//
// LEAVE ENEMY TRAIL
// create a solid sprite the same color as the ENEMY with tile size on enemy position
// call function for animation/fade out
// create a special trail sprite with generated block texture
//
-(void) leaveEnemyTrail:(CGPoint)position {
    
    
    CCSprite* enemyTrailSprite = [CCSprite spriteWithFile:@"whitepixel.png"];
    enemyTrailSprite.textureRect = CGRectMake(0, 0, pixelSize, pixelSize);
    enemyTrailSprite.position = position;//p;
    enemyTrailSprite.color = _enemyColor;
    enemyTrailSprite.opacity = 100;
    enemyTrailSprite.tag = 70;
    
    [mainGameLayer addChild:enemyTrailSprite z:990];
    
    [self trailAnimationForSprite:enemyTrailSprite];
    
    
    if (_testingMode == false) {
        
        subSprite *trailSpriteBlocks;
        
        trailSpriteBlocks = [subSprite spriteWithTexture:[self genTrailTextureWithColor:_enemyColor textureSize:pixelSize sprite:trailSpriteBlocks tiles:4].sprite.texture];
        
        
        trailSpriteBlocks.position = position;
        trailSpriteBlocks.tag = 73;
        
        [mainGameLayer addChild:trailSpriteBlocks z:992];
        
    }
    
}

//
// LEAVE HERO TRAIL
// create a solid sprite the same color as the HERO with tile size on enemy position
// call function for animation/fade out
// create a special trail sprite with generated block texture
//
-(void) leaveTrail:(CGPoint)position {
    
    
    CCSprite* trailSpriteAnimation = [CCSprite spriteWithFile:@"whitepixel.png"];
    trailSpriteAnimation.textureRect = CGRectMake(0, 0, pixelSize, pixelSize);
    trailSpriteAnimation.position = position;//p;
    trailSpriteAnimation.color = _heroColor;
    trailSpriteAnimation.opacity = 100;
    trailSpriteAnimation.tag = 70;
    
    [mainGameLayer addChild:trailSpriteAnimation z:990];
    
    [self trailAnimationForSprite:trailSpriteAnimation];
    
    
    
    
    if (_testingMode == false) {
        
        subSprite *trailSpriteBlocks;
        
        trailSpriteBlocks = [subSprite spriteWithTexture:[self genTrailTextureWithColor:_heroColor textureSize:pixelSize sprite:trailSpriteBlocks tiles:4].sprite.texture];
        
        
        trailSpriteBlocks.position = position;
        trailSpriteBlocks.tag = 72;
        
        [mainGameLayer addChild:trailSpriteBlocks z:992];
        
        
    }
    
    
}

-(void) trailAnimationForSprite:(CCSprite*)sprite {
    
    id fadeAnim = [CCFadeTo actionWithDuration:0.35f opacity:0];
    
    id animationSpawn = [CCSpawn actions:fadeAnim, nil];
    
    id removeAnimSprite = [CCCallFuncN actionWithTarget:self selector:@selector(autoDestroyCollectible:)];
    
    
    id removeSequence = [CCSequence actions: animationSpawn, removeAnimSprite, nil];
    
    
    [sprite runAction:removeSequence];
    
}


//
// UPDATE TRAIL SPRITE BLOCKS CODE
// loop through mainGameLayer children
// if update interval matches trailFrame stored in sprite
// update sprite texture with a new amount of  blocks in the trail sprite based on trailFrame count
//
-(void) updateTrailSpriteBlocks {
    
    if (_testingMode == false) {
        
        
        int updateTrailInterval = 6;
        
        CCArray *loopArray = [[mainGameLayer children] copy];
        
        
        for (id obj in loopArray) {
            
            if ([obj isMemberOfClass:[subSprite class]]) {
                
                
                subSprite *sprite = (subSprite*)obj;
                
                
                if (sprite != NULL && sprite.visible != NO) {
                    
                    
                    
                    if ((sprite.tag == 72) || (sprite.tag == 73) || (sprite.tag == 74) ) { //hero trail blocks
                        
                        
                        
                        int trailFrame = sprite.trailFrame;
                        
                        trailFrame++;
                        
                        int calculateCount = (4-floor(trailFrame/updateTrailInterval));
                        
                        
                        if (calculateCount > 0) {
                            
                            
                            if (sprite.cellsUsedArray.count == calculateCount) {
                                
                                
                            } else {
                                
                                ccColor3B useColor = sprite.savedColor;
                                
                                if (sprite.tag == 72) {
                                    [sprite setTexture:[self genTrailTextureWithColor:_heroColor textureSize:pixelSize sprite:sprite tiles:calculateCount].sprite.texture];
                                    
                                }else if (sprite.tag == 73) {
                                    [sprite setTexture:[self genTrailTextureWithColor:_enemyColor textureSize:pixelSize sprite:sprite tiles:calculateCount].sprite.texture];
                                    
                                }else if (sprite.tag == 74) {
                                    [sprite setTexture:[self genTrailTextureWithColor:useColor textureSize:pixelSize sprite:sprite tiles:calculateCount].sprite.texture];
                                    
                                }
                                
                            }
                            
                            
                            sprite.trailFrame = trailFrame;
                            
                        } else {
                            
                            [sprite removeFromParentAndCleanup:YES];
                            
                        }
                        
                    }
                }
            }
        }
    }
    
    
}


//
// CAPTURE ACCELEROMETER EVENTS
// determine tilt amount and adjust HERO update movement constant based on tilt percentage
// based on tilt, set HERO movement direction
// calibrate accelerometer on start of game, or on return from pause menu
//
- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
    
    if (gameOver == NO){
        
        
        abcx = acceleration.x;
        abcy = acceleration.y;
        abcz = acceleration.z;
        
        
        // if accelerometer default position was not adjusted, do that now
        if(accelUpdated == NO)
            [self accelObStartu];
        
        Vec2 accel2D(0,0);
        Vec3 ax(1, 0, 0);
        
        Vec3 ay(ayx, ayy, ayz);
        
        Vec3 az(Vec3::Cross(ay,ax).normalize());
        ax = Vec3::Cross(az,ay).normalize();
        
        accel2D.x = -Vec3::Dot(Vec3(acceleration.x, acceleration.y, acceleration.z), ax);
        accel2D.y = -Vec3::Dot(Vec3(acceleration.x, acceleration.y, acceleration.z), az);
        
        const float xSensitivity = 2.8f;
        const float ySensitivity = 2.8f; // yay magic numbers!
        const float tiltAmplifier = 10; // one setting was 8
        
        float tiltFactor = 1.0f;
        
        if (ayz > 0.01f) {
            tiltFactor = -1.0f;
        }
        
        
        float accelXx = (accel2D.y) * tiltAmplifier * xSensitivity * tiltFactor;
        float accelYy = -(accel2D.x) * tiltAmplifier * ySensitivity * tiltFactor;
        
        
        
        
        float amountAccel = 2.5f;
        
        float maxAccel = 10.0f;
        
        int lowTimerKonst = 4;
        int highTimerKonst = 6;
        
        float vertMultiplier = 1.2f;
        
        
        if(accelYy > amountAccel*vertMultiplier && (fabsf(accelYy)*(1/vertMultiplier) > fabsf(accelXx))){ // tilting the device upwards
            
            //UP
            
            moveState = MS_UP;
            
            
            //
            
            float tiltPercentage = ((maxAccel-amountAccel)-(min(fabsf(accelYy), maxAccel) - amountAccel)) / (maxAccel-amountAccel);
            
            movementTimerKonst = lowTimerKonst+ceil(highTimerKonst*tiltPercentage);
            
            if (moveUpdateCounter > movementTimerKonst) {
                moveUpdateCounter = 0;
            }
            
            
        } else if (accelYy < -amountAccel*vertMultiplier && (fabsf(accelYy)*(1/vertMultiplier) > fabsf(accelXx))){ // tilting the device downwards
            
            //DOWN
            
            moveState = MS_DOWN;
            
            //
            float tiltPercentage = ((maxAccel-amountAccel)-(min(fabsf(accelYy), maxAccel) - amountAccel)) / (maxAccel-amountAccel);
            
            movementTimerKonst = lowTimerKonst+ceil(highTimerKonst*tiltPercentage);
            
            if (moveUpdateCounter > movementTimerKonst) {
                moveUpdateCounter = 0;
            }
            
            
        } else if(accelXx < -amountAccel && (fabsf(accelXx) > fabsf(accelYy))){  // tilting the device right
            
            //RIGHT
            
            moveState = MS_RIGHT;
            
            
            float tiltPercentage = ((maxAccel-amountAccel)-(min(fabsf(accelXx), maxAccel) - amountAccel)) / (maxAccel-amountAccel);
            
            movementTimerKonst = lowTimerKonst+ceil(highTimerKonst*tiltPercentage);
            
            if (moveUpdateCounter > movementTimerKonst) {
                moveUpdateCounter = 0;
            }
            
            
        } else if (accelXx > amountAccel && (fabsf(accelXx) > fabsf(accelYy))){ // tilting the device left
            
            //LEFT
            
            moveState = MS_LEFT;
            
            //
            float tiltPercentage = ((maxAccel-amountAccel)-(min(fabsf(accelXx), maxAccel) - amountAccel)) / (maxAccel-amountAccel);
            
            movementTimerKonst = lowTimerKonst+ceil(highTimerKonst*tiltPercentage);
            
            if (moveUpdateCounter > movementTimerKonst) {
                moveUpdateCounter = 0;
            }
            
        } else {
            
            moveState = MS_STOP;
            
        }
        
        
        
    }
    
    
}


//
// MOVEMENT OF HERO
// if game is running, check if movement timer matches movement update constant provided from the accelerometer
// based on moveState (from the accelerometer) move HERO one tile in the appropriate direction
// if clipping for HERO is disabled, move to the opposite edge of the mainGameLayer, based on movement
//
-(void) doMove {
    
    int bottomBorder = 0;
    
    CGSize size = CGSizeMake(steviloPoX*pixelSize,steviloPoY*pixelSize);//[CCDirector sharedDirector].winSize;
    
    
    //if gh mode enabled, limit movement to one line
    if (gh == true) {
        bottomBorder = 3*pixelSize;
        size.height = 1*pixelSize;
    } else {
        bottomBorder = 0;
        size.height = steviloPoY*pixelSize;
    }
    
    
    
    CGPoint p = movingSquare.position;
    
    if (gameOver == NO && isStart == YES) {
        
        
        switch (moveState) {
            case MS_UP:
                
                if (p.y < (bottomBorder + size.height-pixelSize/2)) {
                    [self leaveTrail:p];
                    
                    
                    [movingSquare setTexture:[self genTextureWithColor:_heroColor textureSize:movingSquare.contentSize.width sprite:movingSquare direction:1].sprite.texture];
                    
                    [movingSquare setPosition:ccp(p.x, p.y+pixelSize)];
                    
                    previousMoveState = MS_UP;
                    
                } else {
                    
                    if (_heroClip == false) {
                        
                        [self leaveTrail:p];
                        
                        
                        [movingSquare setTexture:[self genTextureWithColor:_heroColor textureSize:movingSquare.contentSize.width sprite:movingSquare direction:1].sprite.texture];
                        [movingSquare setPosition:ccp(p.x,pixelSize/2)];
                        
                        previousMoveState = MS_UP;
                        
                    }
                    
                    previousMoveState = MS_STOP;
                    
                }
                
                break;
                
            case MS_DOWN:
                
                if (p.y > (bottomBorder+pixelSize/2)) {
                    
                    [self leaveTrail:p];
                    
                    [movingSquare setTexture:[self genTextureWithColor:_heroColor textureSize:movingSquare.contentSize.width sprite:movingSquare direction:2].sprite.texture];
                    [movingSquare setPosition:ccp(p.x, p.y-pixelSize)];
                    
                    previousMoveState = MS_DOWN;
                    
                } else {
                    
                    if (_heroClip == false) {
                        
                        [self leaveTrail:p];
                        
                        [movingSquare setTexture:[self genTextureWithColor:_heroColor textureSize:movingSquare.contentSize.width sprite:movingSquare direction:2].sprite.texture];
                        [movingSquare setPosition:ccp(p.x,size.height-pixelSize/2)];
                        
                        previousMoveState = MS_DOWN;
                        
                    }
                    
                    previousMoveState = MS_STOP;
                    
                }
                
                break;
                
            case MS_RIGHT:
                
                if (p.x < size.width-pixelSize/2) {
                    
                    [self leaveTrail:p];
                    
                    [movingSquare setTexture:[self genTextureWithColor:_heroColor textureSize:movingSquare.contentSize.width sprite:movingSquare direction:3].sprite.texture];
                    [movingSquare setPosition:ccp(p.x+pixelSize, p.y)];
                    
                    previousMoveState = MS_RIGHT;
                    
                } else {
                    
                    if (_heroClip == false) {
                        
                        [self leaveTrail:p];
                        
                        [movingSquare setTexture:[self genTextureWithColor:_heroColor textureSize:movingSquare.contentSize.width sprite:movingSquare direction:3].sprite.texture];
                        [movingSquare setPosition:ccp(pixelSize/2,p.y)];
                        
                        previousMoveState = MS_RIGHT;
                        
                    }
                    
                    previousMoveState = MS_STOP;
                    
                }
                
                break;
                
            case MS_LEFT:
                
                if (p.x > pixelSize/2) {
                    
                    [self leaveTrail:p];
                    
                    [movingSquare setTexture:[self genTextureWithColor:_heroColor textureSize:movingSquare.contentSize.width sprite:movingSquare direction:4].sprite.texture];
                    [movingSquare setPosition:ccp(p.x-pixelSize, p.y)];
                    
                    previousMoveState = MS_LEFT;
                    
                } else {
                    
                    if (_heroClip == false) {
                        
                        [self leaveTrail:p];
                        
                        [movingSquare setTexture:[self genTextureWithColor:_heroColor textureSize:movingSquare.contentSize.width sprite:movingSquare direction:4].sprite.texture];
                        [movingSquare setPosition:ccp(size.width-pixelSize/2,p.y)];
                        
                        previousMoveState = MS_LEFT;
                        
                    }
                    
                    previousMoveState = MS_STOP;
                    
                }
                
                break;
                
            default:
                previousMoveState = MS_STOP;
                break;
        }
        
        
        if (moveState != MS_STOP) {
            [self checkClippingHero];
        }
        
    }
    
    
}

#pragma mark POWERUP CODE
//
//POWER UPS
//

//add time to game play in seconds
-(void) addTimeInSeconds:(float)seconds {
    
    gameTimeFrames = gameTimeFrames+roundf(seconds*60);
    
}


//collect all collectibles currently on screen
-(void) collectAllOnScreen {
    
    CCArray *loopArray = [[mainGameLayer children] copy];
    
    for (id obj in loopArray) {
        
        if ([obj isMemberOfClass:[subSprite class]]) {
            
            
            subSprite *sprite = (subSprite*)obj;
            
            if (sprite.tag == 60) {
                
                [self grabCollectible:sprite];
                
            }
            
        }
    }
    
    
    //reset combo data
    
    collectComboCount = 0;
    lastCollectType = 0;
    
    
}


//change hero clipping
-(void) changeHeroClip {
    
    if (_heroClip == false) {
        _heroClip = true;
        
        //
        //if leftover clipping indicators, remove
        //
        
        CCNode *aChild;
        
        while ((aChild = [mainGameLayer getChildByTag:345]) != nil) {
            [aChild removeFromParentAndCleanup:YES];
        }
        
        //
        ///////
        //
        
        
    } else {
        _heroClip = false;
    }
    
}


#pragma mark explode powerup


#define explodeRadius   4

CGPoint explodeCenter;
int explodeStep = 0;


-(void) explodePowerUp {
    
    if (explodeStep == 0) {
        
        //define explosion center
        explodeCenter = movingSquare.position;
        
        [self schedule:@selector(explodeStep) interval:0.05f];
    }
    
    
}

-(void) explodeStep {
    
    //loop through all points on the grid and check if inside circle
    
    float radius = (explodeStep+1.0)*pixelSize;
    
    float radMin = radius-pixelSize/(2+explodeStep*0.1);
    float radMax = radius+pixelSize/(2+explodeStep*0.1);
    
    int radiusExplode = 0;
    
    for (int y = -radiusExplode; y<(steviloPoY+radiusExplode); y++) {
        
        for (int x = -radiusExplode; x<(steviloPoX+radiusExplode); x++) {
            
            CGPoint loopPos = ccp(pixelSize/2+x*pixelSize, pixelSize/2+y*pixelSize);
            
            float distanceApart = ccpDistance(loopPos, explodeCenter);
            
            
            if (distanceApart > radMin && distanceApart < radMax) {
                
                
                //
                //explosion goes through walls - it could, but it won't
                //
                
                
                /*
                 if (loopPos.x < pixelSize/2) {
                 loopPos.x = 480+loopPos.x;
                 } else if (loopPos.x > 480-pixelSize/2) {
                 loopPos.x = 0+(loopPos.x-480);
                 }
                 
                 if (loopPos.y < pixelSize/2) {
                 loopPos.y = 320+loopPos.x;
                 } else if (loopPos.y > 320-pixelSize/2) {
                 loopPos.y = 0+(loopPos.y-320);
                 }
                 */
                
                //
                //////
                //
                
                CCSprite* explodeSprites = [CCSprite spriteWithFile:@"whitepixel.png"];
                explodeSprites.textureRect = CGRectMake(0, 0, pixelSize, pixelSize);
                explodeSprites.position = loopPos;//p;
                explodeSprites.color = ccc3(255-_bgColor1.r,255-_bgColor1.g,255-_bgColor1.b);
                explodeSprites.opacity = 255;
                explodeSprites.tag = 100;
                
                [mainGameLayer addChild:explodeSprites z:950];
                
                [self explodeAnimationForSprite:explodeSprites];
                
            }
            
        }
    }
    
    explodeStep++;
    
    if (explodeStep < explodeRadius) {
        
    } else {
        explodeStep = 0;
        [self unschedule:@selector(explodeStep)];
    }
    
}


-(void) explodeCollisionsForSprite:(subSprite*)collidableSprite {
    
    CCArray *loopArray = [[mainGameLayer children] copy];
    
    
    for (id obj in loopArray) {
        
        if ([obj isMemberOfClass:[CCSprite class]]) {
            
            
            CCSprite *sprite = (CCSprite*)obj;
            
            
            if (sprite != NULL && sprite.visible != NO) {
                
                if (sprite.tag == 100) { // enemies and explosion (explosion je tag 100)
                    
                    
                    float distanceApart = ccpDistance(collidableSprite.position, sprite.position);
                    
                    if (distanceApart < pixelSize) {
                        
                        [self explodeMoveHitEnemy:collidableSprite];
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
}




-(void) explodeMoveHitEnemy:(subSprite*)sprite {
    
    CGPoint dirVec = ccp(0,0);
    // get the angle between point (x1,y1) and (x2,y2)
    
    //float distanceFromExplosionToSprite = ccpDistance(explodeCenter, sprite.position);
    
    float angle = atan2f((sprite.position.x-explodeCenter.x), (sprite.position.y-explodeCenter.y)) * 180/3.14159265;
    
    if (angle > -30 && angle < 30) {
        
        // move up
        dirVec = ccp(0,1);
        
    } else if (angle > -60  && angle < -30) {
        
        //move left/up
        dirVec = ccp(-1,1);
        
    } else if (angle > -120 && angle < -60) {
        
        //move left
        dirVec = ccp(-1,0);
        
    } else if (angle > -150 && angle < -120) {
        
        //move left/down
        dirVec = ccp(-1,-1);
        
    } else if (angle > 30 && angle < 60) {
        
        //move right/up
        dirVec = ccp(1,1);
        
    } else if (angle > 60 && angle < 120) {
        
        //move right
        dirVec = ccp(1,0);
        
    } else if (angle > 120 && angle < 150) {
        
        //move right/down
        
    } else {
        
        //move down
        dirVec = ccp(0,-1);
        
    }
    
    CGPoint curPosition = sprite.position;
    
    CGPoint newPosition = ccp(curPosition.x+dirVec.x*pixelSize,curPosition.y+dirVec.y*pixelSize);
    
    
    if (newPosition.x < pixelSize/2) {
        newPosition.x = 480+newPosition.x;
    } else if (newPosition.x > 480-pixelSize/2) {
        newPosition.x = 0+(newPosition.x-480);
    }
    
    if (newPosition.y < pixelSize/2) {
        newPosition.y = 320+newPosition.x;
    } else if (newPosition.y > 320-pixelSize/2) {
        newPosition.y = 0+(newPosition.y-320);
    }
    
    sprite.position = newPosition;
    
}


-(void) explodeAnimationForSprite:(CCSprite*)sprite {
    
    id fadeAnim = [CCFadeTo actionWithDuration:0.25f opacity:0];
    
    id animationSpawn = [CCSpawn actions:fadeAnim, nil];
    
    id removeAnimSprite = [CCCallFuncN actionWithTarget:self selector:@selector(autoDestroyCollectible:)];
    
    
    id removeSequence = [CCSequence actions: animationSpawn, removeAnimSprite, nil];
    
    
    [sprite runAction:removeSequence];
    
}


#pragma mark freeze powerup

-(void) doFreezeEnemies {
    
    //add code for freezing time
    
}


// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
    
    updateCount = 0;
    updateFollowCount = 0;
    collectibleTimerCount = 0;
    spawnCollectibleTime = minTime;
    timerCount = 0;
    updateEnemyOnCount = 48;
    updateFollowEnemyOnCount = 48;
    
    
    //RESET CLIPPING
    _heroClip = true; //true pomeni, da se zaleti v steno
    
    
    //RESET GUITAR HERO - false by default
    gh = false;
    
    
    //POWERUP STUFF
    explodeStep = 0;
    
    
    //RESET COMBO STATS
    
    collectionsTimeArray = nil;
    
    scoreMult = 1;
    collectComboCount = 0;
    lastCollectType = 0;
    
    gameOver = NO;
    isStart = NO;
    countdownNum = 5;
    countdownCount= 10;
    
    moveUpdateCounter = 0;
    
    scoreCount = 0;
    
    accelUpdated = NO;
    isPause = NO;
    moveState = MS_STOP;
    
}


#pragma mark AutoLockHandler

-(void) changeAutoLock {
    
    if ([[UIApplication sharedApplication] isIdleTimerDisabled]) {
        //enable auto lock
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
        
    } else {
        //disable auto lock
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    }
    
    
    
    
}


#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}
@end
