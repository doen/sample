//
//  GameLayer.h
//  Be A Square
//
//  Created by Ziga Dolar on 5/17/13.
//  Copyright Upalela 2013. All rights reserved.
//


#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"


//subclass of CCSprite
#import "subSprite.h"

//moveStates

enum _moveState {
    MS_STOP,
    MS_LEFT,
    MS_RIGHT,
    MS_UP,
    MS_DOWN
};



//gameModes

enum _gameMode {
    GM_SURVIVE,
    GM_COLLECT,
    GM_CLASSIC
};


// HelloWorldLayer
@interface GameLayer : CCLayer <GKAchievementViewControllerDelegate, GKLeaderboardViewControllerDelegate>
{
    //enum data
    enum _gameMode gameMode;             //game mode states
    enum _moveState moveState;           //hero move states
    
    //main game layer
    CCLayer *mainGameLayer;
    CCLayer *HUDlayer;
    
    
    //global sprites
    CCSprite *_background;          //background
    CCSprite *_backgroundGame;      //background gameLayer
    subSprite *movingSquare;         //hero sprite
    
    //hud elements
    CCLabelTTF *countdownLabel;     //countdown label for hud
    CCLabelTTF *scoreLabel;         //score label for hud
    CCLabelTTF *timerLabel;         //timer label for hud
    
    
    //overlay layers
    CCLayerColor *pauseLayer;
    CCLayerColor *gameOverLayer;
    
    
    //colors
    
    ccColor3B _bgColor1;
    ccColor3B _bgColor2;
    ccColor3B _heroColor;
    ccColor3B _enemyColor;
    ccColor3B _consumableColor;
    
    ccColor3B _hudColor1;
    
    NSString *aId;
    
    
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene:(int)mode;


-(void)doPauseWhenLeavingGame;

@end
