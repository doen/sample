//
//  subSprite.h
//  Be A Square
//
//  Created by doen on 7/2/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface subSprite : CCSprite {
    NSMutableArray *colorArray;
    NSMutableArray *cellsUsedArray;
    int collectibleType;
    int trailFrame;
    ccColor3B savedColor;
}


@property (readwrite) NSMutableArray *colorArray;
@property (readwrite) NSMutableArray *cellsUsedArray;
@property (readwrite) int collectibleType;
@property (readwrite) int trailFrame;
@property (readwrite) ccColor3B savedColor;
@property (readwrite) int subSpriteTime;
@property (readwrite) int subSpriteSpeed;

@end
